#Insurance Application
Aplikacja utworzona na potrzeby małżęństwa chcącego spredawać ubezpieczenia OC na samochody w swoim
 komisie samochodowym.
 ######Słowniczek:
 Za każdym razem gdy pojawi się słowo:
 - widok - mam na myśli stronę html zapisaną w /resources/static.
 - serwis - mam na myśli klasę Javy, która opatrzona jest adnotacją @Service
 - repozytorium - mam na myśli klasę Javy, która opatrzona jest adnotacą @Repository.
 - kontroller - mam nay myśli klasę Javy, która opatrzona jest adnotacją @Controller.
##Zadania Core
###Zadanie 1. 
Pobierz framework spring ze strony initializera. Załącz do niego pakiety Web,
DevTools, Thymeleaf oraz JPA także MySQL jdbc driver. Uruchom aplikację.
### Zadanie 2.   
Za pomocą adnotacji @Bean dołącz do projektu klasę SomeClass, w konstruktorze
której wyświetlisz na konsolę jej nazwę oraz metody, która właśnie jest wykonywana.
Uruchom aplikację.
### Zadanie 3. 
Utwórz jeszcze jedną klasę np. MyClass, tylko tym razem dodaj do niej adnotację
@Component zamiast deklarować ją jako @Bean. Utwórz w niej konstruktor, który na
konsolę wyświetli nazwę klasy oraz metody, która jest właśnie wykonywana. Uruchom
aplikację.
### Zadanie 4. 
W klasie MyClass dodaj pole z adnotacją @Autowired do klasy SomeClass.
Uruchom aplikację.
### Zadanie 5. 
Zadbaj o to, aby klasa SomeClass nie była inicjalizowana podczas startu aplikacji.
### Zadanie 6. 
Utwórz klasę konfiguracyjną z adnotacją @Configuration w pakiecie config i
przenieś wszystkie deklaracje @Bean do niej.
##Zadania Controller
### Zadanie 7. 
Utwórz plik index.html a w nim podstawową treść strony html. Umieść go w
resources/templates. Następnie utwórz klasę kontrolera i dodaj odpowiednie adnotacje tak
aby można było wyświetlić ją w przeglądarce. Uruchom aplikację.
### Zadanie 8. 
Utwórz jeszcze jedną stronę html w katalogu templates. Zrób do niej odnośnik na
stronie głównej. Na drugiej stronie zrób odnośnik powrotny. Poniżej wykonaj licznik
przeładowań strony za pomocą beanów i adnotacji dotyczących scope’ów.
### Zadanie 9. 
Utwórz stronę z linkiem, który prześle 3 parametry: kolor, wielkość, cenę, a na
kolejnej stronie wyświetl tekst: „Kupiłe(a)m {kolor} {wielkość} samochód za {cena} zł”
###Zadanie. 10 
Utwórz stronę z formą, która pobierze imię metodą _POST_, a potem je wyświetli na stronie. 
### Zadanie 11. 
 Na podstawie istniejącego kodu, napisz funkcjonalność, która w beanie będącym
repozytorium będzie zapisywać listę użytkowników dodawanych przez formę metodą post.
Będzie mogła wyświetlić listę takich użykowników.
### Zadanie 12.
 Rozszerz kod napisany w zadaniu 11 tak aby zawierał serwis odpowiedzialny za
sprawdzanie czy taki użytkownik już w istnieje „bazie”.
### Zadanie 13.
 Analogicznie do zadania 11 przepisz aplikację tak, żeby zapisywała dane do bazy
danych.
### Zadanie 14.
 Skonstruuj @Controller tak by wejściu w odpowiedni link aplikacja wyświetliła
uzytkownika o podanym id. na przykład: localhost:8080/user/12 pokaże dane użytkownika o
id 12.
### Zadanie 15.
 Korzystając z kodu napisanego w zadaniu 11. dodaj do formy możliwość dodania
zdjęcia do użytkownika. Następnie wyświetl je obok danych użytkownika. Realizując to
zadanie pamiętaj o relacji 1 użytkownik może mieć więcej niż jedno zdjęcie.Zadania RestController
### Zadanie 16.
 Utwórz nowy projekt w którym utworzyć strukturę bazy: Jak na rysunku:
 ##RestController
### Zadanie 17.
 Napisz funkcjonlaność, która zawróci do klienta surowe dane w postaci JSON. Niech dane zawierają listę użytkowników (klientów)
### Zadanie 18.
 W rest controllerze dopisz metody tak, by można było wybrać pojedynczego
użytkownika za pomocą @PathVariable.
### Zadanie 19.
### Zadanie 20.
 Napisz metodę w kontrolerze, która pozwoli na dodanie nowej osoby w bazie
danych.
### Zadanie 21. 
 Udostępnijcie między sobą swoje adresy Ip i pozwólcie współuczestnikom kursu na dodanie
osoby do waszych baz.
### Zadanie 22. 
Podobnie jak w zadaniu 12 będziemy sprawdzać, czy osoba, którą chcemy wpisać do bazy
już tam istnieje.
### Zadanie 23. 
Za pomocą manipulacji nagłówkami upewnij się że wynik RestControllera będzie
udostępniona w xml a nie w json.
### Zadanie 24. 
Za pomocą biblioteki jQuery dołączonej do projektu (powinna znaleźć się w katalogu
„resources/static”). Wyślij żądanie get do serwera, póki co nie dbamy o odpowiedź.
### Zadanie 25. 
Utwórz formę z 3 inputami, które po wypełnieniu trafią do kontrolera poprzez AJAX.
Kontroler wypisze otrzymane dane na konsolę, a na formie wysłane dane pojawią się obok
inputów.
### Zadanie 26.
 Napisz Serwis, który umożliwi Ci zamianę zdjęcia na base64 i na odwrót.. Wynik
posłuży do przesłania zdjęcia zakodowanego w base64 współuczestnikowi, a druga osoba
za pomocą metody odwrotnej zamieni base64 na obraz.Zadania Obsługa wyjątków
### Zadanie 27.
 W katalogu templates utwórz plik error.html, który będzie podstawowym plikiem
html, a w jego ciele pojawi się tekst: „Wystąpił błąd na stronie”.
### Zadanie. 28
 W dowolnym kontrolerze w dowolnej metodzie, która jest opatrzona adnotacją
@GetMapping przez return, wyrzuć RuntimeException. Uruchom aplikację i
sprawdź efekt.
### Zadanie 29.
 Utwórz nową klasę i nazwij ją MyException. Klasa ta powinna dziedziczyć po
RuntimeException. Zapewnij tej klasie wyjątku odpowiedni status i powód. Teraz wyrzuć
twój wyjątek analogicznie do tego z zadania 26 w innej metodzie.
### Zadanie 30. 
Następnie utwórz nowy widok html o nazwie myError.html z tekstem „Mój wyjątek”, a w
kontrolerze, w którym wyjątek jest rzucany dodaj nową metodę, która nie zwraca nic ale jest
opatrzona adnotacją @ExceptionHandler oraz @ResponseStatus.
### Zadanie 31. 
Utwórz jeszcze jedną klasę o nazwie DontKnowPageException, która dziedziczy po
RuntimeException. Wyrzuć ten wyjątek w kontrolerze. W tym samym kontrolerze napisz
metodę zwracającą String opatrzoną adnotacją @ExceptiuonHadler. Zwracaną wartością
metody będzie „myError”. Uruchom aplikację.
### Zadanie 32. 
Utwórz jeszcze jeden wyjątek na wzór poprzednich. Nazwij go SuperException.
Analogicznie wyrzuc go w metodzie kontrolera, w którym dopiszesz nową metodę z
@ExceptionHandler. Tym razem Metoda ta będzie przyjmować dwa parametry:
HttpServletRequest oraz SuperException a zwracać będzie ModelAndView. Użyj na
ModelAndView metody addObject by dodać wiadmość z wyjątku analogicznie jak robiliście
to w ModelMap. Dodaj teraz nowy widok superException.html gdzie będzie można
wyświetlić dynamicznie treść wyjątku.
### Zadanie 33. 
Utwórz nową klasę, będącą kontrolerem. Zamiast opisać ją jako @Controller użyj adnotacji
@ControllerAdvice. Utwórz też nowy widok megaError.html. W nowej metodzie w
wspomnianym ControllerAdvice dodaj adnotację
@ExceptionHandler( RuntimeException.class). Możesz zakomentować metody, które
zostały napisane do obsługi wyjątków w poprzednich zadaniach. Metoda powinna zwracać
ModelAndViewZadania security
### Zadanie 34. 
Utwórz nowy projekt za pomocą spring initializera (Ze strony producenta), tak jak w zadaniu
1, tylko tym razem zaznacz także „security”.
### Zadanie 35. 
Utwórz kontroler i dwa widoki. Jeden z nich będzie listą samochodów w bazie danych, a
drugi formą dodawania samochodów do bazy danych.
### Zadanie 36.
 Utwórz klasę konfiguracyjną (opatrzoną adnotacją @Configuration). I dodaj do tej klasy
także adnotację @EnableWebSecurity. Uruchom Aplikację i śledź logi.
### Zadanie 37. 
Dopisz do nowo utworzonej klasy, klasę bazową: WebSecurityConfigurerAdapter
### Zadanie 38. 
Utwórz formę logowania i jednocześnie na którymś widoku formę typu post z akcją
„/logout”
### Zadanie 39. 
Utwórz w kontrolerze metodę z @GetMapping(„/login”), w kórej pokażesz formę
logowania.
### Zadanie 40. 
Dodaj użytkownika w pamięci programu. Sprawdź co się zmieniło w logach.
### Zadanie 41. 
Dodaj jeszcze kilku użytkowników i nadaj im różne uprawnienia.
### Zadanie 42.
 Dodaj odpowiednią konfigurację w klasie konfiguracyjnej tak, żeby użytkownik, który
posiada rolę „ADMIN” mógł zobaczyć przycisk/link przenoszący go na stronę z formą aby
dodać samochód do bazy danych.
### Zadanie 43. 
Dodaj odpowiednie mapingi w klasie konfiguracji, tak żeby tylko admin miał prawo
zobaczyć to, co rozpoczyna link od „/admin/**”
### Zadanie 44. 
Utwórz strukturę w bazie danych tak, by można było trzymać tam dane użytkowników
zamiast w pamięci aplikacji.
### Zadanie 45. 
Dodaj implementację Servisu UserDetailsService w klasie UserDetailsServiceImpl i oznacz
ją jako @Service. Przeciąż metodę loadByUserName.
### Zadanie 46. 
Dodaj możliwość rejestracji nowych użytkowników. Po rejestracji wyślij maila z linkiem
potweirdzającym.Zadania mail
### Zadanie 47.
 Utwórz serwis, do którego wstrzykniesz JavaMailSender i wyślesz maila podając parametry
SimpleMail takie jak „to”, „from” „subject” oraz „text”.
### Zadanie 48. 
Z pomocą kontrolera skonstruuj funkcjonalność, która pomoże wysłać użytkownikowi
serwisu maila do Ciebie. Pozwól użytkownikowi wpisać prostą treść w formie oraz określić
temat maila, a także poproś o podanie swojeg