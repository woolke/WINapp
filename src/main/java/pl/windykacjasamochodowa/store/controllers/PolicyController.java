package pl.windykacjasamochodowa.store.controllers;

import org.springframework.stereotype.Controller;
import pl.windykacjasamochodowa.store.services.PoliciesService;

@Controller
public class PolicyController {
    private final PoliciesService policiesService;

    public PolicyController(PoliciesService policiesService) {
        this.policiesService = policiesService;
    }
}
