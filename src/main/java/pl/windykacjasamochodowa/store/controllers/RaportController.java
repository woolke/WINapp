package pl.windykacjasamochodowa.store.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.ServletWebRequest;
import pl.windykacjasamochodowa.store.components.CsvParse;
import pl.windykacjasamochodowa.store.dtos.RaportDto;
import pl.windykacjasamochodowa.store.model._enums.debtorData.AdressSOURCE;
import pl.windykacjasamochodowa.store.model._enums.debtorData.AdressTYPE;
import pl.windykacjasamochodowa.store.model._enums.debtorData.TelephoneSOURCE;
import pl.windykacjasamochodowa.store.model._enums.debtorData.TelephoneTYPE;
import pl.windykacjasamochodowa.store.model._enums.raport.*;
import pl.windykacjasamochodowa.store.model.debt.assigment.Assigment;
import pl.windykacjasamochodowa.store.model.debt.debitors.Adress;
import pl.windykacjasamochodowa.store.model.debt.debitors.Subject;
import pl.windykacjasamochodowa.store.model.debt.debitors.Telephone;
import pl.windykacjasamochodowa.store.model.raportAndComments.Raport;
import pl.windykacjasamochodowa.store.services.*;

import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

import static pl.windykacjasamochodowa.store.components.StaticFunc.*;

@Controller
@RequestMapping("/raport")
public class RaportController {


    private final AssigmentService assigmentService;
    private final SubjectService subjectService;
    private final RaportService raportService;
    private final AdressService adressService;
    private final CarService carService;
    private final TelephoneService telephoneService;
    private final VindicatorServiceImpl vindicatorService;

    @Autowired
    public RaportController(AssigmentService assigmentService, SubjectService subjectService, RaportService raportService, AdressService adressService, CarService carService, TelephoneService telephoneService, VindicatorServiceImpl vindicatorService) {
        this.assigmentService = assigmentService;
        this.subjectService = subjectService;
        this.raportService = raportService;
        this.adressService = adressService;
        this.carService = carService;
        this.telephoneService = telephoneService;
        this.vindicatorService = vindicatorService;
    }


    @GetMapping("/forum")
    public String raportByForum(Model model, String forum, String ajax) {
        Assigment assigment = assigmentService.getByForum(forum);
        ArrayList<RaportDto> dtosToForumRaport = assigment.getDtosToForumRaport();

        addRaportEnumToModel(model);
        addAdressEnumToModel(model);
        addTelephoneEnumToModel(model);
        model.addAttribute("raportList", dtosToForumRaport);

        String returnUrl;
        if (ajax!=null) {
            returnUrl = "raport/forumRaportAjax";
        } else
            returnUrl = "raport/forumRaport";

        return returnUrl;
    }

    @GetMapping("/forumAndAdd")
    public String raportByForumAndAddAdrTel(Model model, String forum) {
        List<Assigment> assigments = assigmentService.getList();


        Assigment assigment = assigmentService.getByForum(forum);
        ArrayList<RaportDto> dtosToForumRaport = assigment.getDtosToForumRaport();

        addRaportEnumToModel(model);
        addAdressEnumToModel(model);
        addTelephoneEnumToModel(model);
        model.addAttribute("raportList", dtosToForumRaport);
        return "raport/forumRaportAndAdd";
    }


    @PostMapping("/save")
    public String save(ServletWebRequest request) {
        ArrayList<Raport> savedRaports = new ArrayList<>();
        Map<String, String[]> parameterMap = request.getParameterMap();
        int form = parameterMap.get("forum").length;
        String forum =parameterMap.get("forum")[0];

        for (int i = 0; i < form; i++) {
            if (!parameterMap.get("raportDate")[i].isEmpty()) {
                if (!parameterMap.get("adress")[i].isEmpty()) {
                    adressRaport(savedRaports, parameterMap, i);
                }

                if (!parameterMap.get("telephone")[i].isEmpty()) {
                    telephoneRaport(savedRaports, parameterMap, i);
                }
            }

        }
        boolean empty = savedRaports.isEmpty();

        return "/raport/forum?forum=" +forum;
    }



    @PostMapping("/saveAndAdd")
    public String saveAndAdd(ServletWebRequest request) {
        ArrayList<Raport> savedRaports = new ArrayList<>();
        Map<String, String[]> parameterMap = request.getParameterMap();
        int form = parameterMap.get("forum").length;

        for (int i = 0; i < form; i++) {
            if (!parameterMap.get("raportDate")[i].isEmpty()) {
                if (!parameterMap.get("adress")[i].isEmpty()) {
                    adressRaport(savedRaports, parameterMap, i);
                }
                if (!parameterMap.get("telephone")[i].isEmpty()) {
                    telephoneRaport(savedRaports, parameterMap, i);
                }
                if (!parameterMap.get("newAdressStreet")[i].isEmpty()) {
                    addNewAdresFromRaport(parameterMap, i);
                }
                if (!parameterMap.get("newTelephone")[i].isEmpty()) {
                    addNewTelephoneFromRaport(parameterMap, i);
                }
            }

        }
        boolean empty = savedRaports.isEmpty();
        String forum =parameterMap.get("forum")[0];
        return "redirect:/";
    }



    private void addNewTelephoneFromRaport(Map<String, String[]> parameterMap, int i) {
        Telephone telephone= Telephone.builder()
                .telephone("+".concat(parameterMap.get("newTelephonePrefix")[i]
                        .concat(parameterMap.get("newTelephone")[i])))
                .telephoneSOURCE(TelephoneSOURCE.valueOf(parameterMap.get("telephoneSOURCE")[i]))
                .telephoneTYPE(TelephoneTYPE.valueOf(parameterMap.get("telephoneTYPE")[i]))
                .comment(parameterMap.get("newTelephoneComment")[i]).build();
        Subject subject =subjectService.getById(parameterMap.get("subject")[i]);
        telephoneService.addTelToSubject(subject, telephone);
        subjectService.save(subject);

    }

    private void addNewAdresFromRaport(Map<String, String[]> parameterMap, int i) {
        Adress adress= Adress.builder()
                .adressZipCode(zipCodeFromXXXXXtoXX_XXX(parameterMap.get("newAdressZip")[i]))
                .adressCity(parameterMap.get("newAdressCity")[i])
                .adressStreet(parameterMap.get("newAdressStreet")[i])
                .adressDoorNumber(parameterMap.get("newAdressHouseNumber")[i])
                .adressHouseNumber(parameterMap.get("newAdressDoorNumber")[i])
                .adressSOURCE(AdressSOURCE.valueOf(parameterMap.get("adressSOURCE")[i]))
                .adressTYPE(AdressTYPE.valueOf(parameterMap.get("adressTYPE")[i]))
                .comment(parameterMap.get("newAdressComment")[i]).build();
        Subject subject =subjectService.getById(parameterMap.get("subject")[i]);
        adressService.addAdressToSubject(subject, adress);
        subjectService.save(subject);

    }

    private void adressRaport(ArrayList<Raport> savedRaports, Map<String, String[]> parameterMap, int i) {
        Raport.RaportBuilder  adressRaport = raportBuilderOrderPart(parameterMap, i);
        raportBuilderCarPart(parameterMap, i, adressRaport);
        Raport raport = raportBuilderAdressPart(parameterMap, i, adressRaport).build();
        raportService.save(raport);
        savedRaports.add(raport);
    }


    private void telephoneRaport(ArrayList<Raport> savedRaports, Map<String, String[]> parameterMap, int i) {
        Raport.RaportBuilder  telRaport = raportBuilderOrderPart(parameterMap, i);
        raportBuilderCarPart(parameterMap, i, telRaport);
        Raport raport = raportBuilderTelephonePart(parameterMap, i, telRaport).build();
        raportService.save(raport);
        savedRaports.add(raport);
    }


    private Raport.RaportBuilder raportBuilderOrderPart(Map<String, String[]> parameterMap, int i) {
        Raport.RaportBuilder raportBuilder = Raport.builder().forum(parameterMap.get("forum")[i])
                .subject(subjectService.getById(parameterMap.get("subject")[i]))
                .raportTYPE(RaportTYPE.valueOf(parameterMap.get("raportTYPE")[i]))
                .raportAMOUNT(RaportAMOUNT.valueOf(parameterMap.get("raportAMOUNT")[i]))
                .raportADRESS(RaportADRESS.valueOf(parameterMap.get("raportADRESS")[i]))
                .raportSTATUS(RaportSTATUS.valueOf(parameterMap.get("raportSTATUS")[i]))
                .declarationPaymantValue(bigDecimalFromString(parameterMap.get("declarationPaymantValue")[i]))
                .declarationPaymantDate(getDateFromStringRRRR_MM_DD(parameterMap.get("declarationPaymantDate")[i]))
                .raportDate(getDateTimeFromStringRRRR_MM_DD_T_HH_MM(parameterMap.get("raportDate")[i]))
                .scheduledTask(getDateTimeFromStringRRRR_MM_DD_T_HH_MM(parameterMap.get("scheduledTask")[i]))
                .comment(parameterMap.get("comment")[i])
                .hideComment(parameterMap.get("hideComment")[i])
                .raportSTATUS2(parameterMap.get("raportSTATUS2")[i])
//                    .priority(Boolean.valueOf(parameterMap.get("priority")[i]))
                .raportTimeStamp(new Date())
                .vindicator(vindicatorService.getLoggedInUser());
        return raportBuilder;
    }

    private Raport.RaportBuilder raportBuilderCarPart(Map<String, String[]> parameterMap, int i, Raport.RaportBuilder builder) {
        Raport.RaportBuilder raportBuilder = builder.car(parameterMap.get("car")[i])
                .raportCAR(RaportCAR.valueOf(parameterMap.get("raportCAR")[i]))
                .declarationRetrieveDate((getDateFromStringRRRR_MM_DD(parameterMap.get("declarationRetrieveDate")[i])));
        return raportBuilder;
    }

    private Raport.RaportBuilder raportBuilderAdressPart(Map<String, String[]> parameterMap, int i, Raport.RaportBuilder builder) {
        Raport.RaportBuilder raportBuilder = builder.adress(adressService.getById(parameterMap.get("adress")[i]))
                .raportADRESS(RaportADRESS.valueOf(parameterMap.get("raportADRESS")[i]));
        return raportBuilder;
    }


    private Raport.RaportBuilder raportBuilderTelephonePart(Map<String, String[]> parameterMap, int i, Raport.RaportBuilder builder) {
        Raport.RaportBuilder raportBuilder = builder
                .telephone(telephoneService.getByNr(parameterMap.get("telephone")[i]))
                .raportTELEPHONE(RaportTELEPHONE.valueOf(parameterMap.get("raportTELEPHONE")[i]));
        return raportBuilder;
    }

    private void addTelephoneEnumToModel(Model model) {
        TelephoneSOURCE[] telephoneSOURCES = TelephoneSOURCE.values();
        TelephoneTYPE[] telephoneTYPES = TelephoneTYPE.values();
        model.addAttribute("telephoneSOURCES", telephoneSOURCES );
        model.addAttribute("telephoneTYPES", telephoneTYPES );
    }

    private void addAdressEnumToModel(Model model) {
        AdressSOURCE[] adressSOURCES = AdressSOURCE.values();
        AdressTYPE[] adressTYPES = AdressTYPE.values();
        model.addAttribute("adressSOURCES", adressSOURCES );
        model.addAttribute("adressTYPES", adressTYPES );
    }

    private void addRaportEnumToModel(Model model) {
        RaportTYPE[] raportTYPES = RaportTYPE.values();
        RaportAMOUNT[] raportAMOUNTS = RaportAMOUNT.values();
        RaportADRESS[] raportADRESSES = RaportADRESS.values();
        RaportSTATUS[] raportSTATUS = RaportSTATUS.values();
        RaportTELEPHONE[] raportTELEPHONES = RaportTELEPHONE.values();
        RaportCAR[] raportCARS = RaportCAR.values();
        model.addAttribute("raportAMOUNTS", raportAMOUNTS);
        model.addAttribute("raportTYPES", raportTYPES);
        model.addAttribute("raportADRESES", raportADRESSES);
        model.addAttribute("raportSTATUSS", raportSTATUS);
        model.addAttribute("raportCARS", raportCARS);
        model.addAttribute("raportTELEPHONES", raportTELEPHONES);
    }


}
