package pl.windykacjasamochodowa.store.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import pl.windykacjasamochodowa.store.components.AddAssigment;
import pl.windykacjasamochodowa.store.components.PdfBox;
import pl.windykacjasamochodowa.store.services.RaportService;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import static pl.windykacjasamochodowa.store.components.CsvParse.multipartToinputStream;

@Controller
@RequestMapping("/upload")
public class UploadController {
    private final AddAssigment addAssigment;
    private final PdfBox pdfBox;
    private final RaportService raportService;


    @Autowired
    public UploadController(AddAssigment addAssigment, PdfBox pdfBox, RaportService raportService) {
        this.addAssigment = addAssigment;
        this.pdfBox = pdfBox;
        this.raportService = raportService;
    }

    @GetMapping("/kiArchiveRaport")
    public String kiArchiveRaport() {
        return "upload/kiRaportFromArchiveCsv";
    }

    @PostMapping("/kiArchiveRaport")
    public String kiArchiveRaport(Model model, @RequestParam(value = "uploadfile", required = false) MultipartFile multipartFile) {
        InputStream inputStream = multipartToinputStream(multipartFile);
        Map<String, String> unsaveRaport = raportService.saveRaportFromArchive(inputStream);
        Set<Map.Entry<String, String>> unsave = unsaveRaport.entrySet();

        model.addAttribute("unsave", unsave);

        for (Map.Entry<String, String> stringStringEntry : unsaveRaport.entrySet()) {
            stringStringEntry.getKey();
            stringStringEntry.getValue();
        }
        return "tmp/tableUnSaveAssigment";
    }


    @GetMapping("/kiRetrive")
    public String index() {
        return "upload/kiRetriveOrderCsv";
    }

    @PostMapping("/kiRetrive")
    public String makeCounterZero(Model model, @RequestParam(value = "uploadfile", required = false) MultipartFile multipartFile) {
        InputStream inputStream = multipartToinputStream(multipartFile);
        Map<String, String> unsaveAssiggment = addAssigment.KiRetrive(inputStream);
        Set<Map.Entry<String, String>> unsave = unsaveAssiggment.entrySet();

        model.addAttribute("unsave", unsave);

        for (Map.Entry<String, String> stringStringEntry : unsaveAssiggment.entrySet()) {
            stringStringEntry.getKey();
            stringStringEntry.getValue();
        }
        return "tmp/tableUnSaveAssigment";
    }

    @GetMapping("/policyPdf")
    public String pdfUpload() {
        return "upload/policyPdf";
    }

    @PostMapping("/policyPdf")
    public String postPdfUpload(Model model, @RequestParam(value = "uploadfile", required = false) MultipartFile multipartFile) {
        InputStream inputStream = multipartToinputStream(multipartFile);
        ArrayList<String> pdfFile = pdfBox.readPdf(inputStream);
        model.addAttribute("pdfLine", pdfFile);
        return "pdf/printPdfLine";
    }
}
