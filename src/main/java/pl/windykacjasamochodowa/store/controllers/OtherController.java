package pl.windykacjasamochodowa.store.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import pl.windykacjasamochodowa.store.components.PdfBox;
import pl.windykacjasamochodowa.store.components.ReadWebsite;

import java.io.InputStream;
import java.util.ArrayList;

import static pl.windykacjasamochodowa.store.components.CsvParse.multipartToinputStream;

@Controller
@RequestMapping("/other")
public class OtherController {

    private final PdfBox pdfBox;
    private final ReadWebsite readWebsite;


    @Autowired
    public OtherController(PdfBox pdfBox, ReadWebsite readWebsite) {
        this.pdfBox = pdfBox;
        this.readWebsite = readWebsite;
    }



    @GetMapping ("/readwww")
    public String readwww(Model model, String url) {
        try {
//            String urlSource = readWebsite.getURLSource(url);
            String urlSource = readWebsite.readWWWofCertValidation(url);
            model.addAttribute("urlSource", urlSource);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "readwww/index";
    }
}