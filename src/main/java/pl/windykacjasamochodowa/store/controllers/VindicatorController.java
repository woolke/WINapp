package pl.windykacjasamochodowa.store.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.windykacjasamochodowa.store.model.debt.assigment.Vindicator;
import pl.windykacjasamochodowa.store.services.VindicatorServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/user")
public class VindicatorController {

    private final VindicatorServiceImpl vindicatorService;

    @Autowired
    public VindicatorController(VindicatorServiceImpl vindicatorService) {
        this.vindicatorService = vindicatorService;
    }

    @GetMapping("/register")
    public String showAddNewUser(Model model) {
        model.addAttribute("vindicator", new Vindicator());
        return "user/add";
    }

    @PostMapping("/save")
    public String saveUser(@ModelAttribute("vindicator") Vindicator vindicator) {
        vindicatorService.save(vindicator);
        return "redirect:/";
    }

    @GetMapping(value = "/login")
    public String login(Model model, String error, String logout, RedirectAttributes ra) {
        return "login";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/";
    }

}




