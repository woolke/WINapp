package pl.windykacjasamochodowa.store.controllers;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;
import pl.windykacjasamochodowa.store.components.*;
import pl.windykacjasamochodowa.store.dtos.RaportDto;
import pl.windykacjasamochodowa.store.model._enums.debtorData.AdressSOURCE;
import pl.windykacjasamochodowa.store.model._enums.debtorData.AdressTYPE;
import pl.windykacjasamochodowa.store.model._enums.debtorData.AdressVERIFIED;
import pl.windykacjasamochodowa.store.model._enums.raport.*;
import pl.windykacjasamochodowa.store.model.debt.assigment.Assigment;
import pl.windykacjasamochodowa.store.model.debt.debitors.Adress;
import pl.windykacjasamochodowa.store.model.debt.debitors.Debtor;
import pl.windykacjasamochodowa.store.model.debt.debitors.Location;
import pl.windykacjasamochodowa.store.model.debt.debitors.Telephone;
import pl.windykacjasamochodowa.store.model.debt.order.Car;
import pl.windykacjasamochodowa.store.model.raportAndComments.Raport;
import pl.windykacjasamochodowa.store.respositories.debt.assigment.AssigmentRepository;
import pl.windykacjasamochodowa.store.respositories.debt.debitors.AdressRepository;
import pl.windykacjasamochodowa.store.respositories.debt.order.CarRepository;
import pl.windykacjasamochodowa.store.services.AssigmentService;
import pl.windykacjasamochodowa.store.services.LocationService;
import pl.windykacjasamochodowa.store.services.RaportService;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import static pl.windykacjasamochodowa.store.services.TelephoneService.prettyPrintTelephone;

@Controller
@RequestMapping("/test")
public class TestController {

    /**
     * Adnotacji @Autowired można też użyć przy polu (nie tylko w konstruktorze).
     * Instancja klasy MyClass zostanie ustawiona zaraz po wykonaniu konstruktora
     */
private final PdfBox pdfBox;
    private final CarRepository carRepository;
    private final AssigmentService assigmentService;
    private final AssigmentRepository assigmentRepository;
    private final AdressRepository adressRepository;
    private final LocationService locationService;
    private final AddAssigment addAssigment;
    private final RaportService raportService;
    private  final ZipWinApp zipWinApp;

    @Autowired
    public TestController(PdfBox pdfBox, CarRepository carRepository, AssigmentService assigmentService, AssigmentRepository assigmentRepository, AdressRepository adressRepository, LocationService locationService, AddAssigment addAssigment, RaportService raportService, ZipWinApp zipWinApp) {
        this.pdfBox = pdfBox;
        this.carRepository = carRepository;
        this.assigmentService = assigmentService;
        this.assigmentRepository = assigmentRepository;
        this.adressRepository = adressRepository;
        this.locationService = locationService;
        this.addAssigment = addAssigment;
        this.raportService = raportService;
        this.zipWinApp = zipWinApp;
    }


    /**
     * Adnotacja @GetMapping mówi springowi,ze jeżeli do naszego serwera przyjdzie żądanie
     * http GET, to spring ma wywołać właśnie tę metodę.
     */
    @GetMapping("/addSampleData")
    public String CounterZero() {

        String s = StaticFunc.zipCodeFromXXXXXtoXX_XXX("44153");

        Adress adress = Adress.builder()
                .adressSOURCE(AdressSOURCE.ZE_ZLECENIA)
                .adressTYPE(AdressTYPE.GLOWNY)
                .adressVERIFIED(AdressVERIFIED.ZWERYFIKOWANY_DLUGOTRWALA)
                .adressZipCode("32-740")
                .adressFull("32-740 KOBYLEC 188")
                .location(Location.builder().id(locationService.getGeoPointFromAddress("32-740 KOBYLEC 188")).build())
                .build();
        adressRepository.save(adress);


        return "redirect:/";
    }

    @GetMapping ("/assigmentTest")
    public String assigmentTest (Model model) {
        List<Assigment> findByVindicator_Id = assigmentRepository.findByVindicator_Id(10);
//        List<Assigment> findByCampaign = assigmentRepository.findByCampaign(campaign);
//        Optional<Assigment> findByOrder_Forum = assigmentRepository.findByOrder_Forum("770794");
//        List<Assigment> findByOrder_ForumAndVindicator_Id = assigmentRepository.findByOrder_ForumAndVindicator_Id(String order_forum, Integer vindicator_id);
//        Optional<Assigment> findFirstByOrder_ForumAndVindicator_IdOrderByIdDesc = assigmentRepository.findFirstByOrder_ForumAndVindicator_IdOrderByIdDesc(String order_forum, Integer vindicator_id);
//        Optional<Assigment> findByCampaignNameAndOrderForum = assigmentRepository.findByCampaignNameAndOrderForum(String campaign_name, String order_forum);
//        List<Assigment> findByVindicator_IdAndCampaign_CampaignEndGreaterThan = assigmentRepository.findByVindicator_IdAndCampaign_CampaignEndGreaterThan(Integer id, Date date);
//        List<Assigment> findByCampaignCampaignEnd_GreaterThan = assigmentRepository.findByCampaignCampaignEnd_GreaterThan(Date date);
    return "/";
    }


    @GetMapping ("getByForumCampaign")
        public String getByForumCampaignTest (Model model, String forum, String campaign) {
        Optional<Assigment> byCampaignNameAndOrderForum = assigmentRepository.findByCampaignNameAndOrderForum(campaign, forum);
        model.addAttribute("assigment", byCampaignNameAndOrderForum.orElse(null));
        return byCampaignNameAndOrderForum.toString();
    }

    @GetMapping("/addRaportForum")
    public String addRaport(Model model, String forum) {

        List<Assigment> assigmentList = assigmentRepository.findAll();
        Assigment assigment = assigmentList.get(0);


        List<String> forumList = assigmentList.stream()
                .map(adress -> adress.getOrder().getForum()).collect(Collectors.toList());

        List<RaportDto> raportList = new ArrayList<>();
        for (Debtor debtor : assigment.getOrder().getDebtors()) {
            RaportDto tmpRow = RaportDto.builder()
                    .forum(assigment.getOrder().getForum())
                    .subject(debtor.getSubject())
                    .car(assigment.getOrder().getCar().getNrRej())
//                    .car(assigment.getOrder().getMortgage().getNrKW())
                    .build();
            raportList.add(tmpRow);
        }


        RaportTYPE[] raportTYPES = RaportTYPE.values();
        RaportAMOUNT[] raportAMOUNTS = RaportAMOUNT.values();
        RaportADRESS[] raportADRESSES = RaportADRESS.values();
        RaportSTATUS[] raportSTATUSS = RaportSTATUS.values();
        RaportCAR[] raportCARS = RaportCAR.values();
        model.addAttribute("raportAMOUNTS", raportAMOUNTS);
        model.addAttribute("raportTYPES", raportTYPES);
        model.addAttribute("raportEFFECTS", raportADRESSES);
        model.addAttribute("raportSTATUSS", raportSTATUSS);
        model.addAttribute("raportCARS", raportCARS);
        model.addAttribute("raportList", raportList);
        model.addAttribute("forumList", forumList);

        return "orderRaport";
    }


    @PostMapping("/save")
    public String save(WebRequest request) {
        Map<String, String[]> parameterMap = request.getParameterMap();

        return "car/list";
    }

    @GetMapping("/upload")
    public String index() {
        return "tmp/uploadform.html";
    }

    @PostMapping("/upload")
    public String makeCounterZero(Model model, @RequestParam(value = "uploadfile", required = false) MultipartFile multipartFile) {

        List<Car> cars = CsvParse.csvToCarParse(CsvParse.multipartToinputStream(multipartFile));
        carRepository.saveAll(cars);
        return "car/list";
    }



    @GetMapping("/orderList")
    public String orderList(Model model) {
        List<Assigment> assigmentList = assigmentService.getList();
        model.addAttribute("assigments", assigmentList);
        return "tmp/table";
    }

    @GetMapping("/orderList2")
    public String orderList2(Model model) {
        List<Assigment> assigmentList = assigmentService.getList();
        model.addAttribute("assigments", assigmentList);
        return "tmp/table2";
    }

    @GetMapping("/archiveList")
    public String archiveList(Model model) {
        List<Assigment> assigmentList = assigmentService.getAll();
        model.addAttribute("assigments", assigmentList);
        return "tmp/table2";
    }


    @GetMapping("/url")
    public String link(Model model, String url) {
        return "tmp/".concat(url);
    }

    @GetMapping("/test")
    public String allLink(Model model, String url) {
        return url;
    }

    @GetMapping("/printTelephone")
    public String printTelephone(Model model, String tel) {
        return prettyPrintTelephone(Telephone.builder().telephone("+48" +tel).build());
    }


    @GetMapping("/printRaport")
    public String printRaport(Model model, String id) {
        Raport byId = raportService.getById(Integer.parseInt(id));
        String s = byId.fullPrintString();
        model.addAttribute("string", s);
        model.addAttribute("newLineChar", '\n');
        return "tmp/printRaport";
    }

    @GetMapping("/editPdf")
    public String pdfEdit(Model model, String url) {
        try {
            PdfBox.update();
        } catch (IOException e) {

        }
        return "redriect:/".concat(url);
    }



    @GetMapping("/geoadres")
    public String geoadres(Model model, String adress) {
        Point geoPointFromAddress = locationService.getGeoPointFromAddress(adress);
        model.addAttribute("point", geoPointFromAddress);
        model.addAttribute("string", adress);
        return "tmp/geoadress";
    }

    @GetMapping("/csvki")
    public String csvki(Model model, String nazwa) {
        InputStream csv = null;
        try {
            csv = new FileInputStream(nazwa);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        addAssigment.KiRetrive(csv);

        return "tmp/geoadress";
    }



    @GetMapping("/selectenum")
    public String selectEnum(ModelMap model) {


        java.util.List<AdressVERIFIED> enumValues = Arrays.asList(AdressVERIFIED.values());
        model.addAttribute("enums", enumValues);
//        enumValues.get(1).getFullName()
        return "tmp/select";
    }

    /**
     * Adnotacja @GetMapping mówi springowi,ze jeżeli do naszego serwera przyjdzie żądanie
     * http GET, to spring ma wywołać właśnie tę metodę.
     * <p>
     * Parametr ModelMap jest mapą wartości, które możemy odczytać po stronie htmla.
     */
    @GetMapping("/set")
    public String showSetCounter(ModelMap model) {
        return "form";
    }


    @GetMapping("/showlist")
    public String showList(ModelMap model) {
        List<String> names = Arrays.asList("Adrian", "Damian", "Jola", "Dawid", "Dominika", "Karol");
        model.addAttribute("names", names);
        return "list";
    }

    @GetMapping("/tmp")
    public String shoeTmp(ModelMap model) {
        List<Assigment> assigmentList = assigmentRepository.findAll();
        model.addAttribute("assigments", assigmentList);
        return "tmp/tmp";
    }

    @GetMapping("/zipStructure")
    public String zip(ModelMap model, String forum) {
        try {
            Map<String, String> structure = zipWinApp.getStructure(forum);
            model.addAttribute("map", structure);
            model.addAttribute("forum", forum);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "zip/showStructure";
    }

    @GetMapping("/zipEntry")
    public void zipEntry(HttpSession session, HttpServletResponse response, String path, String forum) {
        HashMap<String, Object> stringObjectHashMap = null;
        try {
            stringObjectHashMap = zipWinApp.readFile(path, forum);

        String extention = (String) stringObjectHashMap.get("extention");
            InputStream inputStream = (InputStream) stringObjectHashMap.get("inputStream");

//        response.setContentType("application/force-download");
        response.setHeader("Content-Disposition", "attachment; filename="+path);
        IOUtils.copy(inputStream, response.getOutputStream());
        response.flushBuffer();
        inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
