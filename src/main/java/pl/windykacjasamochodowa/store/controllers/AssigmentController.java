package pl.windykacjasamochodowa.store.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import pl.windykacjasamochodowa.store.components.AddAssigment;
import pl.windykacjasamochodowa.store.model.debt.assigment.Assigment;
import pl.windykacjasamochodowa.store.model.debt.assigment.Vindicator;
import pl.windykacjasamochodowa.store.model.debt.order.Balance;
import pl.windykacjasamochodowa.store.model.raportAndComments.Raport;
import pl.windykacjasamochodowa.store.services.AssigmentService;
import pl.windykacjasamochodowa.store.services.RaportService;
import pl.windykacjasamochodowa.store.services.VindicatorServiceImpl;

import java.io.InputStream;
import java.util.*;

import static pl.windykacjasamochodowa.store.components.CsvParse.multipartToinputStream;

@Controller
@RequestMapping("/assigment")
public class AssigmentController {

    private final AssigmentService assigmentService;
    private final AddAssigment addAssigment;
    private final VindicatorServiceImpl vindicatorService;
    private final RaportService raportService;

    @Autowired
    public AssigmentController(AssigmentService assigmentService, AddAssigment addAssigment, VindicatorServiceImpl vindicatorService, RaportService raportService) {
        this.assigmentService = assigmentService;
        this.addAssigment = addAssigment;

        this.vindicatorService = vindicatorService;
        this.raportService = raportService;
    }


    @GetMapping ("/forum")
    public String singleForumView(Model model, String forum) {
        Assigment assigment = assigmentService.getByForum(forum);
        Balance lastBalance = new Balance();
        List<Raport> raports = raportService.getByAllSubject(assigment);
        try {
            lastBalance = assigment.getOrder().getBalances().stream().max(Comparator.comparing(Balance::getBalanceDate)).get();
        } catch (Exception e) {
        }

// Debtor debtor; Adress adress; Telephone telephone; Email email;
//debtor.getSubject().getEmails()
//        telephone.getTelephoneTYPE().getFullName();
//email.getEmail()


        model.addAttribute("assigment", assigment);
        model.addAttribute("lastBalance", lastBalance);
        model.addAttribute("raports", raports);

        return "order/retrive/singleForum";
    }




    @GetMapping("/wtlist")
    public String wtlist(Model model) {
        Vindicator loggedInUser = vindicatorService.getLoggedInUser();
        List<Assigment> wt = assigmentService.getList();

        model.addAttribute("assigments", wt);
        return "tmp/table";
    }



}