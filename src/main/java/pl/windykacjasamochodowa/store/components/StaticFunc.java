package pl.windykacjasamochodowa.store.components;

import org.springframework.stereotype.Component;
import pl.windykacjasamochodowa.store.model.debt.debitors.Telephone;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;

@Component
public class StaticFunc {
    public static Date getDateFromStringRRRR_MM_DD(String stringDate) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd");
        Date parse = null;
        if (!(stringDate == null)) {
            try {
                parse = simpleDateFormat.parse(stringDate);
            } catch (ParseException e) {
                return null;
            }
        }

        return parse;
    }

    public static Date getDateTimeFromStringRRRR_MM_DD_T_HH_MM(String stringDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        Date parse = null;
        try {
            parse = simpleDateFormat.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parse;
    }


    public static BigDecimal bigDecimalFromString(String string) {

        boolean warunek = (string.isEmpty());
        if (string.isEmpty()) {
            return BigDecimal.valueOf(0.0);
        } else {
            return BigDecimal.valueOf(Double.parseDouble(string.replace(',', '.')
            .replace(" ","")));
        }
    }

    public static int intFromStringID(String string) {
        boolean warunek = (string.isEmpty() || string == null);
        if (string.isEmpty()) {
            return 0;
        } else {
            return Integer.parseInt(string);
        }
    }

    public static String zipCodeFromXXXXXtoXX_XXX(String zipCode) {
        return zipCode.substring(0, 2) + "-" + zipCode.substring(2, 5);
    }



}
