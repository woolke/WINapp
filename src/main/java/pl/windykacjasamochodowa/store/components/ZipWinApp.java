package pl.windykacjasamochodowa.store.components;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Paths;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

@Component
public class ZipWinApp {

    public void getZip(String path) throws Exception {
        String filePath = "C:\\WINapp\\".concat(path).concat(".zip");
        ZipFile zip = new ZipFile(filePath);
        getStructure(filePath);

        for (Enumeration e = zip.entries(); e.hasMoreElements(); ) {
            ZipEntry entry = (ZipEntry) e.nextElement();
            if (!entry.isDirectory()) {
                if (FilenameUtils.getExtension(entry.getName()).equals("png")) {
                    byte[] image = getImage(zip.getInputStream(entry));
                    //do your thing
                } else if (FilenameUtils.getExtension(entry.getName()).equals("txt")) {
                    StringBuilder out = getTxtFiles(zip.getInputStream(entry));
                    //do your thing
                }
            }
        }


    }

    public Map<String, String> getStructure(String zipFile) throws Exception {
        Map <String, String> structure = new HashMap<>();
        ZipFile file = unlockArchive(zipFile);

        Enumeration<? extends ZipEntry> entries = file.entries();
        while ( entries.hasMoreElements() )
        {
            final ZipEntry entry = entries.nextElement();
            if (!entry.isDirectory()) {
                structure.put(entry.toString(), Paths.get(entry.getName()).getFileName().toString());
            }

        }


        return structure;
    }

    public HashMap<String, Object> readFile (String path, String forum) throws Exception {
        ZipFile zipFile = unlockArchive(forum);
        ZipEntry entry = zipFile.getEntry(path);
        HashMap<String, Object> map = new HashMap<>();
        zipFile.getInputStream(entry);
        map.put("inputStream", zipFile.getInputStream(entry));
        map.put("extention", FilenameUtils.getExtension(entry.getName()));
        return map;
    }

    private ZipFile unlockArchive(String forum) {

        String filePath = "C:\\WINapp\\".concat(forum).concat(".zip");

        try {
         return new ZipFile(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static StringBuilder getTxtFiles(InputStream in) {
        StringBuilder out = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
        } catch (IOException e) {
            // do something, probably not a text file
            e.printStackTrace();
        }
        return out;
    }

    private static byte[] getImage(InputStream in) {
        try {
            BufferedImage image = ImageIO.read(in); //just checking if the InputStream belongs in fact to an image
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(image, "png", baos);
            return baos.toByteArray();
        } catch (IOException e) {
            // do something, it is not a image
            e.printStackTrace();
        }
        return null;
    }

}
