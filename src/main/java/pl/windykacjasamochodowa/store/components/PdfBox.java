package pl.windykacjasamochodowa.store.components;

import org.apache.pdfbox.pdfparser.PDFStreamParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class PdfBox {

    public ArrayList<String> readPdf(InputStream pdfFile) {

        try (PDDocument document = PDDocument.load(pdfFile)) {

            document.getClass();

            if (!document.isEncrypted()) {

                PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                stripper.setSortByPosition(true);

                PDFTextStripper tStripper = new PDFTextStripper();

                String pdfFileInText = tStripper.getText(document);
                //System.out.println("Text:" + st);

                // split by whitespace
                String lines[] = pdfFileInText.split("\\r?\\n");
                ArrayList<String> pdfArrayLines = new ArrayList<>();
                for (String line : lines) {
                    pdfArrayLines.add(line);
                }
                return pdfArrayLines;

            }

        } catch (InvalidPasswordException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void update() throws InvalidPasswordException, IOException {
        Map<String, String> map = new HashMap<>();
        map.put("fieldname", "value to update");
        File template = new File("testPdf.pdf");
        PDDocument document = PDDocument.load(template);
        List<PDField> fields = document.getDocumentCatalog().getAcroForm().getFields();
        for (PDField field : fields) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                if (entry.getKey().equals(field.getFullyQualifiedName())) {
                    field.setValue(entry.getValue());
                    field.setReadOnly(true);
                }
            }
        }
        File out = new File("out.pdf");
        document.save(out);
        document.close();
    }

    public static void replacePdf() throws IOException {
//        PDDocument doc = null;
//        try {
//            doc = PDDocument.load("testPdf.pdf"); //Input PDF File Name
//            List pages = doc.getDocumentCatalog().getAllPages();
//            for (int i = 0; i < pages.size(); i++) {
//                PDPage page = (PDPage) pages.get(i);
//                PDStream contents = page.getContents();
//                PDFStreamParser parser = new PDFStreamParser(contents.getStream());
//                parser.parse();
//                List tokens = parser.getTokens();
//                for (int j = 0; j < tokens.size(); j++) {
//                    Object next = tokens.get(j);
//                    if (next instanceof PDFOperator) {
//                        PDFOperator op = (PDFOperator) next;
//                        // Tj and TJ are the two operators that display strings in a PDF
//                        if (op.getOperation().equals("Tj")) {
//                            // Tj takes one operator and that is the string
//                            // to display so lets update that operator
//                            COSString previous = (COSString) tokens.get(j - 1);
//                            String string = previous.getString();
//                            string = string.replaceFirst("Solr", "Solr123");
//                            //Word you want to change. Currently this code changes word "Solr" to "Solr123"
//                            previous.reset();
//                            previous.append(string.getBytes("ISO-8859-1"));
//                        } else if (op.getOperation().equals("TJ")) {
//                            COSArray previous = (COSArray) tokens.get(j - 1);
//                            for (int k = 0; k < previous.size(); k++) {
//                                Object arrElement = previous.getObject(k);
//                                if (arrElement instanceof COSString) {
//                                    COSString cosString = (COSString) arrElement;
//                                    String string = cosString.getString();
//                                    string = string.replaceFirst("Solr", "Solr123");
//                                    // Currently this code changes word "Solr" to "Solr123"
//                                    cosString.reset();
//                                    cosString.append(string.getBytes("ISO-8859-1"));
//                                }
//                            }
//                        }
//                    }
//                }
//                // now that the tokens are updated we will replace the page content stream.
//                PDStream updatedStream = new PDStream(doc);
//                OutputStream out = updatedStream.createOutputStream();
//                ContentStreamWriter tokenWriter = new ContentStreamWriter(out);
//                tokenWriter.writeTokens(tokens);
//                page.setContents(updatedStream);
//            }
//            doc.save("a.pdf"); //Output file name
//        } finally {
//            if (doc != null) {
//                doc.close();
//            }
//        }
//
    }

}