package pl.windykacjasamochodowa.store.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.windykacjasamochodowa.store.model._enums.WinPARTNER;
import pl.windykacjasamochodowa.store.model._enums.debtorData.*;
import pl.windykacjasamochodowa.store.model.debt.assigment.Assigment;
import pl.windykacjasamochodowa.store.model.debt.assigment.Campaign;
import pl.windykacjasamochodowa.store.model.debt.assigment.Vindicator;
import pl.windykacjasamochodowa.store.model.debt.debitors.Adress;
import pl.windykacjasamochodowa.store.model.debt.debitors.Debtor;
import pl.windykacjasamochodowa.store.model.debt.debitors.Subject;
import pl.windykacjasamochodowa.store.model.debt.debitors.Telephone;
import pl.windykacjasamochodowa.store.model.debt.order.Balance;
import pl.windykacjasamochodowa.store.model.debt.order.Car;
import pl.windykacjasamochodowa.store.model.debt.order.Order;
import pl.windykacjasamochodowa.store.model.debt.order.OrderType;
import pl.windykacjasamochodowa.store.services.*;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static pl.windykacjasamochodowa.store.components.StaticFunc.bigDecimalFromString;
import static pl.windykacjasamochodowa.store.components.StaticFunc.getDateFromStringRRRR_MM_DD;

@Component
public class AddAssigment {
    private final AssigmentService assigmentService;
    private final OrderService orderService;
    private final VindicatorService vindicatorService;
    private final CampaignService campaignService;
    private final OrderTypeService orderTypeService;
    private final AdressService adressService;
    private final SubjectService subjectService;
    private final TelephoneService telephoneService;
    private final DebtorService debtorService;
    private final CarService carService;


    @Autowired
    public AddAssigment(AssigmentService assigmentService, OrderService orderService, VindicatorService vindicatorService, CampaignService campaignService, OrderTypeService orderTypeService, AdressService adressService, SubjectService subjectService, DebtorService debtorService, TelephoneService telephoneService, CarService carService) {
        this.assigmentService = assigmentService;
        this.orderService = orderService;
        this.vindicatorService = vindicatorService;
        this.campaignService = campaignService;
        this.orderTypeService = orderTypeService;
        this.adressService = adressService;
        this.subjectService = subjectService;
        this.debtorService = debtorService;
        this.telephoneService = telephoneService;
        this.carService = carService;
    }


    public Map<String, String> KiRetrive(InputStream csvInuptStream) {
        ArrayList<Map<String, String>> mapFromKiRetriveCsv = CsvParse.getMapFromKiRetriveCsv(csvInuptStream);
        HashMap<String, String> unsaveAssigment = new HashMap<>();
        for (Map<String, String> csvRow : mapFromKiRetriveCsv) {

            try {
                Assigment assigmentRow = addKiRetriveSingleRow(csvRow);
                assigmentService.save(assigmentRow);

            } catch (Exception e) {
                unsaveAssigment.put(csvRow.get("forum"),e.getMessage());
                System.out.println(" Nie utworzono zlecenia: "
                        + csvRow.get("forum") +", " + e.getMessage());

            }


        }

        return (unsaveAssigment);
    }

    private Assigment addKiRetriveSingleRow(Map<String, String> csvRow) {
        Vindicator wT = vindicatorService.getById
                (csvRow.get("idWt"));
        Campaign campaign = campaignService.getOrAdd(
                (csvRow.get("campaign")), (csvRow.get("orderStart")), (csvRow.get("orderEnd")));
        Order order = adKiRetriveOrder(csvRow);


        Subject subjectKb = addKiRetriveKb(csvRow);
        Debtor debtorKb = debtorService.addSubjectToForum(subjectKb, DebtorTYPE.mappingAndValueOf(csvRow.get("debtorTYPE")), DebtorROLE.KB);
        order.getDebtors().add(debtorKb);

        if (!csvRow.get("nameWkb").equals("")) {
            Subject subjectWkb = addKiRetriveWbk(csvRow);
            Debtor debtorWkb = debtorService.addSubjectToForum(subjectWkb, DebtorTYPE.ND, DebtorROLE.WP);
            order.getDebtors().add(debtorWkb);


        }
        if (!csvRow.get("namePor").equals("")) {
            Subject subjectP = addKiRetrivePor(csvRow);
            Debtor debtorP = debtorService.addSubjectToForum(subjectP, DebtorTYPE.ND, DebtorROLE.P);
            order.getDebtors().add(debtorP);
        }
        Assigment assigment = assigmentService.getByForumAndCampaign(order.getForum(), campaign.getName());
        assigment.setVindicator(wT);
        assigment.setCampaign(campaign);
//                .orderStart() /todo
//                .orderEnd() /todo
        assigment.setOrder(order);
        return assigment;
    }

    private Subject addKiRetriveKb(Map<String, String> csvRow) {
        Subject subject = subjectService.addByStringAndType
                (csvRow.get("nameKb"), csvRow.get("peselRegon"), DebtorTYPE.mappingAndValueOf(csvRow.get("debtorTYPE")));
        Adress adressZam = adressService.buildAdress
                ((csvRow.get("adressZamKb")),(csvRow.get("adressZipCode")), AdressTYPE.ZAMELDOWANIA,
                        AdressSOURCE.ZE_ZLECENIA);
        Adress adressKor = adressService.buildAdress
                ((csvRow.get("adressKorKb")), (csvRow.get("adressZipCode")),AdressTYPE.KORESPONDENCYJNY,
                        AdressSOURCE.ZE_ZLECENIA);
        Telephone telephone1 = telephoneService.buildFromNrAndTypeSource
                (csvRow.get("telephone1"), TelephoneSOURCE.ZE_ZLECENIA, TelephoneTYPE.KOMORKOWY);
        Telephone telephone2 = telephoneService.buildFromNrAndTypeSource
                (csvRow.get("telephone2"), TelephoneSOURCE.ZE_ZLECENIA, TelephoneTYPE.KOMORKOWY);
        Telephone telephone3 = telephoneService.buildFromNrAndTypeSource
                (csvRow.get("telephone3"), TelephoneSOURCE.ZE_ZLECENIA, TelephoneTYPE.KOMORKOWY);

        if (!csvRow.get("adressZamKb").isEmpty()) {
            adressService.addAdressToSubject(subject, adressZam);
        }
        if (!csvRow.get("adressKorKb").isEmpty()) {
            adressService.addAdressToSubject(subject, adressKor);
        }
        if (!csvRow.get("telephone1").isEmpty()) {
            telephoneService.addTelToSubject(subject, telephone1);
        }
        if (!csvRow.get("telephone2").isEmpty()) {
            telephoneService.addTelToSubject(subject, telephone2);
        }
        if (!csvRow.get("telephone3").isEmpty()) {
            telephoneService.addTelToSubject(subject, telephone3);
        }
        subjectService.save(subject);
        return subject;
    }




    private Subject addKiRetrivePor(Map<String, String> csvRow) {

        Subject subject = subjectService.addByStringAndType
                (csvRow.get("namePor"), "brakPor".concat(csvRow.get("forum")), DebtorTYPE.ND);
        Adress adressZam = adressService.buildAdress
                (AdressTYPE.ZAMELDOWANIA, AdressSOURCE.ZE_ZLECENIA, (csvRow.get("adressPor")));
        adressService.addAdressToSubject(subject, adressZam);
        subjectService.save(subject);
        return subject;
    }


    private Subject addKiRetriveWbk(Map<String, String> csvRow) {
        Subject subject = subjectService.addByStringAndType
                (csvRow.get("nameWkb"),"brakWkb".concat(csvRow.get("forum")), DebtorTYPE.ND);
        Adress adressZam = adressService.buildAdress
                (AdressTYPE.ZAMELDOWANIA, AdressSOURCE.ZE_ZLECENIA, (csvRow.get("adressWkb")));
        adressService.addAdressToSubject(subject, adressZam);
        subjectService.save(subject);
        return subject;
    }


    private Order adKiRetriveOrder(Map<String, String> csvRow) {

        Order order = orderService.getByIdOrBlank(csvRow.get("forum"));

        order.setVariant(csvRow.get("variant"));
        order.setOrginalCreditor(csvRow.get("orginalCreditor"));
        order.setOrginalNumer(csvRow.get("orginalNumer"));
        order.setOrginalIBAN(csvRow.get("orginalIBAN"));
        order.setSigningDate(getDateFromStringRRRR_MM_DD(csvRow.get("signingDate")));
        order.setTerminationDate(getDateFromStringRRRR_MM_DD(csvRow.get("terminationDate")));
        order.setActualIBAN(csvRow.get("actualIBAN"));
        order.setOrderComments(csvRow.get("orderComments"));
        order.setWinPARTNER(WinPARTNER.valueOf(csvRow.get("winPARTNER")));
        Balance balance = KiRetriveGetBalance(csvRow);
        balance.setOrder(order);
        order.getBalances().add(balance);
        order.setCar(KiRetriveGetCar(csvRow));

        OrderType orderType = orderTypeService.getByName(csvRow.get("orderType"));
        order.setOrderType(orderType);
        return order;
    }

    private Balance KiRetriveGetBalance(Map<String, String> csvRow) {
        return Balance.builder()
                .balance(bigDecimalFromString(csvRow.get("balance")))
                .capital(bigDecimalFromString(csvRow.get("capital")))
                .interest(bigDecimalFromString(csvRow.get("interest")))
                .costs(bigDecimalFromString(csvRow.get("costs")))
                .balanceDate(getDateFromStringRRRR_MM_DD(csvRow.get("orderStart"))).build();


    }

    private Car KiRetriveGetCar (Map<String, String> csvRow) {
        Car car = carService.getByNrRej(csvRow.get("nrRej"));
        if (car==null) {
            car= new Car();
        }
        car.setBrand(csvRow.get("brand"));
        car.setNrRej(csvRow.get("nrRej"));
        car.setProductionYear(csvRow.get("productionYear"));
        car.setVin(csvRow.get("vin"));
        car.setAdditionalComments(csvRow.get("carAdditionalComment"));
        car.setFirstRejDate(getDateFromStringRRRR_MM_DD(csvRow.get("firstRejDate")));
        car.setContractOfTransfer(csvRow.get("contractOfTransfer"));
        car.setTerminationDoc(csvRow.get("terminationDoc"));
        car.setConfirmationOfReceipt(csvRow.get("confirmationOfReceipt"));
        return car;
    }

}
