package pl.windykacjasamochodowa.store.components;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import pl.windykacjasamochodowa.store.model.debt.order.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class CsvParse {

    public static InputStream multipartToinputStream (MultipartFile multipartFile) {
        try {
            InputStream inputStream = multipartFile.getInputStream();
        return  inputStream;
        } catch (IOException e) {
            return null;
        }

    }
        public static List<Car> csvToCarParse(InputStream inputStream) {
        CSVParser parser = getCsvParser(inputStream, ';');

        List<Car> carList = new ArrayList<Car>();
        for (CSVRecord record : parser) {
            Car car = Car.builder().brand(getValueIfColumnNameExist(record, "marka"))
                    .nrRej(getValueIfColumnNameExist(record, "rej"))
                    .productionYear(getValueIfColumnNameExist(record, "rok"))
                    .vin(getValueIfColumnNameExist(record, "vin"))
                    .type(getValueIfColumnNameExist(record, "type"))
                    .model(getValueIfColumnNameExist(record, "model"))
                    .oldNrRej(getValueIfColumnNameExist(record, "currentrej"))
                    .firstRejDate(StaticFunc.getDateFromStringRRRR_MM_DD(getValueIfColumnNameExist(record, "firstrejdate")))
                    .contractOfTransfer(getValueIfColumnNameExist(record, "przywlaszczenie"))
                    .terminationDoc(getValueIfColumnNameExist(record, "wypowiedzenie"))
                    .confirmationOfReceipt(getValueIfColumnNameExist(record, "zwrotka")).build();
            carList.add(car);
        }
        try {
            parser.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return carList;

    }

    public static ArrayList<Map<String, String>> getMapRaportFromArchiveCsv(InputStream inputStream) {
        CSVParser parser = getCsvParser(inputStream, ';');
        ArrayList<Map<String, String>> assigmentList = new ArrayList<Map<String,String>>();
        for (CSVRecord record : parser) {

            HashMap<String, String> row = new HashMap<>();
            row.put("id", getValueIfColumnNameExist(record, "id"));
            row.put("date", getValueIfColumnNameExist(record, "date"));
            row.put("type", getValueIfColumnNameExist(record, "type"));
            row.put("ustalenie", getValueIfColumnNameExist(record, "ustalenie"));
            row.put("car", getValueIfColumnNameExist(record, "car"));
            row.put("comment", getValueIfColumnNameExist(record, "comment"));
            row.put("idWt", getValueIfColumnNameExist(record, "idWt"));
            row.put("forum", getValueIfColumnNameExist(record, "forum"));
            assigmentList.add(row);
            System.out.println();

        }
        try {
            parser.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return assigmentList;

    }

    public static ArrayList<Map<String, String>> getMapFromKiRetriveCsv(InputStream inputStream) {
        CSVParser parser = getCsvParser(inputStream, ';');
        ArrayList<Map<String, String>> assigmentList = new ArrayList<Map<String,String>>();
        for (CSVRecord record : parser) {

            HashMap<String, String> row = new HashMap<>();
            row.put("variant", getValueIfColumnNameExist(record, "variant"));
            row.put("orginalCreditor", getValueIfColumnNameExist(record, "orginalCreditor"));
            row.put("winPARTNER", getValueIfColumnNameExist(record, "winPARTNER"));
            row.put("orderType", getValueIfColumnNameExist(record, "orderType"));
            row.put("idWt", getValueIfColumnNameExist(record, "idWt"));
            row.put("orderStart", getValueIfColumnNameExist(record, "orderStart"));
            row.put("orderEnd", getValueIfColumnNameExist(record, "orderEnd"));
            row.put("campaign", getValueIfColumnNameExist(record, "campaign"));
            row.put("forum", getValueIfColumnNameExist(record, "forum"));
            row.put("orginalNumer", getValueIfColumnNameExist(record, "orginalNumer"));
            row.put("orginalIBAN", getValueIfColumnNameExist(record, "orginalIBAN"));
            row.put("signingDate", getValueIfColumnNameExist(record, "signingDate"));
            row.put("nameKb", getValueIfColumnNameExist(record, "nameKb"));
            row.put("peselRegon", getValueIfColumnNameExist(record, "peselRegon"));
            row.put("adressZamKb", getValueIfColumnNameExist(record, "adressZamKb"));
            row.put("adressZipCode", getValueIfColumnNameExist(record, "adressZipCode"));
            row.put("adressKorKb", getValueIfColumnNameExist(record, "adressKorKb"));
            row.put("telephone1", getValueIfColumnNameExist(record, "telephone1"));
            row.put("telephone2", getValueIfColumnNameExist(record, "telephone2"));
            row.put("telephone3", getValueIfColumnNameExist(record, "telephone3"));
            row.put("terminationDate", getValueIfColumnNameExist(record, "terminationDate"));
            row.put("debtorTYPE", getValueIfColumnNameExist(record, "debtorTYPE"));
            row.put("nameWkb", getValueIfColumnNameExist(record, "nameWkb"));
            row.put("adressWkb", getValueIfColumnNameExist(record, "adressWkb"));
            row.put("namePor", getValueIfColumnNameExist(record, "namePor"));
            row.put("adressPor", getValueIfColumnNameExist(record, "adressPor"));
            row.put("brand", getValueIfColumnNameExist(record, "brand"));
            row.put("nrRej", getValueIfColumnNameExist(record, "nrRej"));
            row.put("productionYear", getValueIfColumnNameExist(record, "productionYear"));
            row.put("vin", getValueIfColumnNameExist(record, "vin"));
            row.put("carAdditionalComment", getValueIfColumnNameExist(record, "carAdditionalComment"));
            row.put("type", getValueIfColumnNameExist(record, "type"));
            row.put("actualIBAN", getValueIfColumnNameExist(record, "actualIBAN"));
            row.put("balance", getValueIfColumnNameExist(record, "balance"));
            row.put("capital", getValueIfColumnNameExist(record, "capital"));
            row.put("interest", getValueIfColumnNameExist(record, "interest"));
            row.put("costs", getValueIfColumnNameExist(record, "costs"));
            row.put("orderComments", getValueIfColumnNameExist(record, "orderComments"));
            row.put("firstRejDate", getValueIfColumnNameExist(record, "firstRejDate"));
            row.put("contractOfTransfer", getValueIfColumnNameExist(record, "contractOfTransfer"));
            row.put("terminationDoc", getValueIfColumnNameExist(record, "terminationDoc"));
            row.put("confirmationOfReceipt", getValueIfColumnNameExist(record, "confirmationOfReceipt"));
            assigmentList.add(row);
            System.out.println();

        }
        try {
            parser.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return assigmentList;

    }

    public static ArrayList<Map<String, String>> getMapFromCeidgCsv(InputStream inputStream) {
        CSVParser parser = getCsvParser(inputStream, ';');
        ArrayList<Map<String, String>> assigmentList = new ArrayList<Map<String,String>>();
        for (CSVRecord record : parser) {

            HashMap<String, String> row = new HashMap<>();
            row.put("id", getValueIfColumnNameExist(record, "Identyfikator wpisu"));
            row.put("firstName", getValueIfColumnNameExist(record, "Imię"));
            row.put("lastname", getValueIfColumnNameExist(record, "Nazwisko"));
            row.put("nip", getValueIfColumnNameExist(record, "NIP"));
            row.put("regon", getValueIfColumnNameExist(record, "REGON"));
            row.put("company", getValueIfColumnNameExist(record, "Firma"));
            row.put("email", getValueIfColumnNameExist(record, "Email"));
            row.put("telephone", getValueIfColumnNameExist(record, "Telefon"));
            row.put("mainAdrCity", getValueIfColumnNameExist(record, "Adres główny - miejscowość"));
            row.put("mainAdrStreet", getValueIfColumnNameExist(record, "Adres główny - ulica"));
            row.put("mainAdrBuilding", getValueIfColumnNameExist(record, "Adres główny - budynek"));
            row.put("mainAdrLocal", getValueIfColumnNameExist(record, "Adres główny - lokal"));
            row.put("mainAdrZip", getValueIfColumnNameExist(record, "Adres główny - kod pocztowy"));
            row.put("mainAdrPost", getValueIfColumnNameExist(record, "Adres główny - poczta"));
            row.put("mainAdrPostContener", getValueIfColumnNameExist(record, "Adres główny - skrytka pocztowa"));
            row.put("mainAdrCountry", getValueIfColumnNameExist(record, "Adres główny - kraj"));
            row.put("mainAdrPowiat", getValueIfColumnNameExist(record, "Adres główny - powiat"));
            row.put("mainAdrGmina", getValueIfColumnNameExist(record, "Adres główny - gmina"));
            row.put("Adres do doręczeń - miejscowość", getValueIfColumnNameExist(record, "Adres do doręczeń - miejscowość"));
            row.put("Adres do doręczeń - ulica", getValueIfColumnNameExist(record, "Adres do doręczeń - ulica"));
            row.put("Adres do doręczeń - budynek", getValueIfColumnNameExist(record, "Adres do doręczeń - budynek"));
            row.put("Adres do doręczeń - lokal", getValueIfColumnNameExist(record, "Adres do doręczeń - lokal"));
            row.put("Adres do doręczeń - kod pocztowy", getValueIfColumnNameExist(record, "Adres do doręczeń - kod pocztowy"));
            row.put("Adres do doręczeń - poczta", getValueIfColumnNameExist(record, "Adres do doręczeń - poczta"));
            row.put("Adres do doręczeń - skrytka pocztowa", getValueIfColumnNameExist(record, "Adres do doręczeń - skrytka pocztowa"));
            row.put("Adres do doręczeń - kraj", getValueIfColumnNameExist(record, "Adres do doręczeń - kraj"));
            row.put("Adres do doręczeń - powiat", getValueIfColumnNameExist(record, "Adres do doręczeń - powiat"));
            row.put("Adres do doręczeń - gmina", getValueIfColumnNameExist(record, "Adres do doręczeń - gmina"));
            row.put("Status", getValueIfColumnNameExist(record, "Status"));
            row.put("Kody PKD", getValueIfColumnNameExist(record, "Kody PKD"));
            row.put("Data rozpoczęcia", getValueIfColumnNameExist(record, "Data rozpoczęcia"));
            row.put("Data zawieszenia", getValueIfColumnNameExist(record, "Data zawieszenia"));
            row.put("Data wznowienia", getValueIfColumnNameExist(record, "Data wznowienia"));
            row.put("Data zaprzestania", getValueIfColumnNameExist(record, "Data zaprzestania"));
            row.put("Data wykreślenia", getValueIfColumnNameExist(record, "Data wykreślenia"));
            assigmentList.add(row);
            System.out.println();

        }
        try {
            parser.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return assigmentList;

    }

    private static CSVParser getCsvParser(InputStream inputStream, char delimiter) {
        CSVFormat format = CSVFormat.EXCEL.withHeader().withDelimiter(delimiter);
        CSVParser parser = null;
        try {
            InputStreamReader csvFile = new InputStreamReader(inputStream);
            parser = new CSVParser(csvFile, format);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return parser;
    }

    private static String getValueIfColumnNameExist(CSVRecord record, String columnName) {
        if (record.isMapped(columnName)) {
            return record.get(columnName);
        } else {
            return null;
        }
    }

}

