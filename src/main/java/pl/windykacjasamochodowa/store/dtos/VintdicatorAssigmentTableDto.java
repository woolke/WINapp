package pl.windykacjasamochodowa.store.dtos;

import pl.windykacjasamochodowa.store.model._enums.WinPARTNER;
import pl.windykacjasamochodowa.store.model.debt.assigment.Campaign;
import pl.windykacjasamochodowa.store.model.debt.assigment.Vindicator;
import pl.windykacjasamochodowa.store.model.debt.debitors.Subject;
import pl.windykacjasamochodowa.store.model.debt.order.*;
import pl.windykacjasamochodowa.store.model.raportAndComments.OrderComment;

import java.util.Date;
import java.util.Set;

public class VintdicatorAssigmentTableDto {
//    from AssigmentClass
    Integer AssigmentId;
    Vindicator vindicator;
    Order order;
    Campaign campaign;
    Date orderStart;
    Date orderEnd;
    Date retriveDeclaration;
    Date scheduledContact;

//    from OrderClass
    String forum;
    Set<Subject> debtors;
    Set<Payment> payments;
    Set<Balance> balances;
    Set<OrderDetails> orderDetails;
    Set<OrderComment> orderComments;
    Car car;
    Set<OcCheck> ocChecks;
    Mortgage mortgage;
    WinPARTNER winPARTNER;
    String orginalCreditor;
    String orginalIBAN;
    String actualIBAN;
    String variant;
    Date signingDate;
    Date terminationDate;
    Date maturityDate2;

}
