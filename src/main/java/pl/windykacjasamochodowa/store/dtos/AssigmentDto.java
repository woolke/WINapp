package pl.windykacjasamochodowa.store.dtos;

import lombok.*;
import pl.windykacjasamochodowa.store.model.debt.assigment.Assigment;
import pl.windykacjasamochodowa.store.model.debt.assigment.Campaign;
import pl.windykacjasamochodowa.store.model.debt.assigment.Vindicator;
import pl.windykacjasamochodowa.store.model.debt.debitors.Debtor;
import pl.windykacjasamochodowa.store.model.debt.order.Order;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter

public class AssigmentDto {
    Integer id;
    Vindicator vindicator;
    Order order;
    Campaign campaign;
    Date orderStart;
    Date orderEnd;
    Date declarationRetrieveDate;
    Date declarationPaymantDate;
    Date scheduledTask;
    BigDecimal declarationPaymantValue;

    public AssigmentDto createFromAssigment (Assigment assigment) {
        this.id= assigment.getId();
        this.vindicator= assigment.getVindicator();
        this.order= assigment.getOrder();
        this.campaign= assigment.getCampaign();
        this.orderStart= assigment.getOrderStart();
        this.orderEnd= assigment.getOrderEnd();
        this.declarationRetrieveDate= assigment.getDeclarationRetrieveDate();
        this.declarationPaymantDate= assigment.getDeclarationPaymantDate();
        this.scheduledTask= assigment.getScheduledTask();
        this.declarationPaymantValue= assigment.getDeclarationPaymantValue();
        return this;
    }


}
