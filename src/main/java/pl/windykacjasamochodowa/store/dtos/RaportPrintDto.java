package pl.windykacjasamochodowa.store.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RaportPrintDto {
    String forum;
    String subject;
    String debtorRole;
    String vindicator;
    String raportDate;
    String declarationPaymantDate;
    String scheduledTask;
    String adress;
    String telephone;
    String declarationPaymantValue;
    String raportTYPE;
    String raportADRESS;
    String raportAMOUNT;
    String raportSTATUS;
    String raportSTATUS2;
    String comment;
    String hideComment;
    String car;
    String raportCAR;
    String declarationRetrieveDate;


    public String publicRaportPrint() {
        return forum + '\'' +
                ", subject='" + subject + '\'' +
                ", debtorRole='" + debtorRole + '\'' +
                ", vindicator='" + vindicator + '\'' +
                ", raportDate='" + raportDate + '\'' +
                ", declarationPaymantDate='" + declarationPaymantDate + '\'' +
                ", scheduledTask='" + scheduledTask + '\'' +
                ", adress='" + adress + '\'' +
                ", telephone='" + telephone + '\'' +
                ", declarationPaymantValue='" + declarationPaymantValue + '\'' +
                ", raportTYPE='" + raportTYPE + '\'' +
                ", raportADRESS='" + raportADRESS + '\'' +
                ", raportAMOUNT='" + raportAMOUNT + '\'' +
                ", raportSTATUS='" + raportSTATUS + '\'' +
                ", raportSTATUS2='" + raportSTATUS2 + '\'' +
                ", comment='" + comment + '\'' +
                ", hideComment='" + hideComment + '\'' +
                ", car='" + car + '\'' +
                ", raportCAR='" + raportCAR + '\'' +
                ", declarationRetrieveDate='" + declarationRetrieveDate + '\'' +
                '}';

//        2018-03-19 | forum: 768932 ADAM NOWAK|(2007 - Hyundai Coupe - SGL 6JA1)  Deklaracja Wydania pojazdu na dzień : 2018-01-23 | (00-200 Warszawa  ul Jakaśtam 20 )  AdresNieaktualny | (689 123 945) Nie Odbiera | Spłata Całości ( w kwocie 3000 do dnia 2018-01-23 )| oifvjodfivjsdfovjsdfovisdjfvoisdjfvosdifjvsodfijvsodfivjfvsdfovijsdfoivjsdfvid (osdjvaosvijasdovajsdovij) WNIOSEK O UGODĘ - WŁĄSNY STATUS
    }
}
