package pl.windykacjasamochodowa.store.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import pl.windykacjasamochodowa.store.model._enums.raport.*;
import pl.windykacjasamochodowa.store.model.debt.assigment.Vindicator;
import pl.windykacjasamochodowa.store.model.debt.debitors.*;
import pl.windykacjasamochodowa.store.model.debt.order.Order;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@Builder
public class RaportDto {
    Order order;
    String forum;
    Subject subject;
    String debtorRole;
    Vindicator vindicator;
    Date raportDate;
    Date declarationPaymantDate;
    Date scheduledTask;
    Date raportTimeStamp;
    Adress adress;
    Telephone telephone;
    Email email;
    BigDecimal declarationPaymantValue;
    RaportTYPE raportTYPE;
    RaportADRESS raportADRESS;
    RaportAMOUNT raportAMOUNT;
    RaportSTATUS raportSTATUS;
    String raportSTATUS2;
    Boolean priority;
    String comment;
    String hideComment;
    Boolean hasMortgage;
    String car;
    RaportCAR raportCAR;
    Date declarationRetrieveDate;
}
