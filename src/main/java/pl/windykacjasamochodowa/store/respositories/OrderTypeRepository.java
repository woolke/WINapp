package pl.windykacjasamochodowa.store.respositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.windykacjasamochodowa.store.model.debt.order.OrderType;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderTypeRepository extends JpaRepository <OrderType, Integer>{
    Optional<OrderType> findByName (String name);
}
