package pl.windykacjasamochodowa.store.respositories.retrieve;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.windykacjasamochodowa.store.model.retrieve.LegalDefect;
import pl.windykacjasamochodowa.store.model.retrieve.Policy;

@Repository
public interface LegalDefectRepository extends JpaRepository<LegalDefect, Integer> {

}
