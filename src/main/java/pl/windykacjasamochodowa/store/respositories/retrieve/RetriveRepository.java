package pl.windykacjasamochodowa.store.respositories.retrieve;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.windykacjasamochodowa.store.model.retrieve.Retrive;

@Repository
public interface RetriveRepository extends JpaRepository<Retrive, Integer> {

}
