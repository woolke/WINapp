package pl.windykacjasamochodowa.store.respositories.retrieve;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.windykacjasamochodowa.store.model.retrieve.Policy;

@Repository
public interface PolicyRepository extends JpaRepository<Policy, Integer> {

}
