package pl.windykacjasamochodowa.store.respositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.windykacjasamochodowa.store.model.Role;

@Repository
public interface RolesRepository extends JpaRepository<Role, Integer> {
}
