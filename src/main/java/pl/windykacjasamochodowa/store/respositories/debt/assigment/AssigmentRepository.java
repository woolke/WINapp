package pl.windykacjasamochodowa.store.respositories.debt.assigment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.windykacjasamochodowa.store.model.debt.assigment.Assigment;
import pl.windykacjasamochodowa.store.model.debt.assigment.Campaign;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface AssigmentRepository extends JpaRepository <Assigment, Integer> {
    List<Assigment> findByVindicator_Id(Integer id);
    List<Assigment> findByCampaign(Campaign campaign);
    Optional<Assigment> findTopByOrder_Forum(String forum);
    List<Assigment> findByOrder_ForumAndVindicator_Id(String order_forum, Integer vindicator_id);
    Optional<Assigment> findFirstByOrder_ForumAndVindicator_IdOrderByIdDesc(String order_forum, Integer vindicator_id);

    Optional<Assigment> findByCampaignNameAndOrderForum(String campaign_name, String order_forum);
    List<Assigment> findByVindicator_IdAndCampaign_CampaignEndGreaterThan(Integer id, Date date);
    List<Assigment> findByCampaignCampaignEnd_GreaterThan(Date date);
}
