package pl.windykacjasamochodowa.store.respositories.debt.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.windykacjasamochodowa.store.model.debt.order.OcCheck;

@Repository
public interface OcCheckRapository extends JpaRepository<OcCheck, Integer> {

}
