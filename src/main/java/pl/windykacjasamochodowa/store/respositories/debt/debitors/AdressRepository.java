package pl.windykacjasamochodowa.store.respositories.debt.debitors;

        import org.springframework.data.jpa.repository.JpaRepository;
        import org.springframework.stereotype.Repository;
        import pl.windykacjasamochodowa.store.model.debt.debitors.Adress;

        import java.util.List;

@Repository
public interface AdressRepository extends JpaRepository <Adress, Integer> {
    List<Adress> findByAdressFull(String adressFull);
}
