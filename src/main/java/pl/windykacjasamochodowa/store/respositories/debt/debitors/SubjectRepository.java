package pl.windykacjasamochodowa.store.respositories.debt.debitors;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.windykacjasamochodowa.store.model.debt.debitors.Subject;

import java.util.Optional;

@Repository
public interface SubjectRepository extends JpaRepository <Subject, Integer> {
    Optional<Subject> findByRegon(String regon);

    Optional<Subject> findByNip(String nip);

    Optional<Subject> findByPesel(String pesel);
}
