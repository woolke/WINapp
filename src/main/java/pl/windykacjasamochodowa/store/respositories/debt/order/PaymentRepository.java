package pl.windykacjasamochodowa.store.respositories.debt.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.windykacjasamochodowa.store.model.debt.order.Order;
import pl.windykacjasamochodowa.store.model.debt.order.Payment;

import java.util.List;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Integer> {
    List<Payment> findByOrder(Order order) ;
}
