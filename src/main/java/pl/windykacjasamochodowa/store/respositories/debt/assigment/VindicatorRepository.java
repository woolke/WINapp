package pl.windykacjasamochodowa.store.respositories.debt.assigment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.windykacjasamochodowa.store.model.debt.assigment.Vindicator;

@Repository
public interface VindicatorRepository extends JpaRepository <Vindicator, Integer> {
    Vindicator findByUsername(String username);
}
