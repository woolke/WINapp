package pl.windykacjasamochodowa.store.respositories.debt.debitors;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.windykacjasamochodowa.store.model.debt.debitors.Telephone;

import java.util.Optional;

@Repository
public interface TelephoneRepository extends JpaRepository<Telephone, Integer> {
    Optional<Telephone> findByTelephone (String telephoneNr);
}
