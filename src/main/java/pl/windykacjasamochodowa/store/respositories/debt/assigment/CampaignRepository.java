package pl.windykacjasamochodowa.store.respositories.debt.assigment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.windykacjasamochodowa.store.model.debt.assigment.Campaign;

import java.util.Optional;

@Repository
public interface CampaignRepository extends JpaRepository <Campaign, String> {
    Optional<Campaign> findByName(String name);
}
