package pl.windykacjasamochodowa.store.respositories.debt.debitors;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.windykacjasamochodowa.store.model.debt.debitors.Email;

@Repository
public interface EmailRepository extends JpaRepository<Email, Integer> {

}
