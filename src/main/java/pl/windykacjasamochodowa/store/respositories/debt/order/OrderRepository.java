package pl.windykacjasamochodowa.store.respositories.debt.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.windykacjasamochodowa.store.model.debt.order.Order;
import pl.windykacjasamochodowa.store.model.debt.order.OrderDetails;

@Repository
public interface OrderRepository extends JpaRepository<Order, String> {

}
