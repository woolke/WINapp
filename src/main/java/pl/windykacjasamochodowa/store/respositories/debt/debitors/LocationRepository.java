package pl.windykacjasamochodowa.store.respositories.debt.debitors;

import org.springframework.data.geo.Point;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.windykacjasamochodowa.store.model.debt.debitors.Location;


@Repository
public interface LocationRepository extends JpaRepository <Location, Point>{
}
