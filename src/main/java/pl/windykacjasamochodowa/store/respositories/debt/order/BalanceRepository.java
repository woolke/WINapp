package pl.windykacjasamochodowa.store.respositories.debt.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.windykacjasamochodowa.store.model.debt.order.Balance;
import pl.windykacjasamochodowa.store.model.debt.order.Order;

import java.util.List;
import java.util.Optional;

@Repository
public interface BalanceRepository extends JpaRepository<Balance, Integer> {

    List<Balance> findByOrder(Order order);
    List<Balance> findByOrder_Forum(String forum);
    Optional<Balance> findTopByOrder_Forum(String forum);
}
