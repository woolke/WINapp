package pl.windykacjasamochodowa.store.respositories.debt.debitors;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.windykacjasamochodowa.store.model.debt.debitors.Debtor;
import pl.windykacjasamochodowa.store.model.debt.debitors.Subject;

import java.util.Optional;

@Repository
public interface DebtorRepository extends JpaRepository <Debtor, Integer> {
    Optional<Debtor> findBySubject(Subject subject);

}
