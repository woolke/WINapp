package pl.windykacjasamochodowa.store.respositories.raportAndComments;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.windykacjasamochodowa.store.model.debt.assigment.Vindicator;
import pl.windykacjasamochodowa.store.model.debt.debitors.Subject;
import pl.windykacjasamochodowa.store.model.raportAndComments.Raport;
import pl.windykacjasamochodowa.store.model.retrieve.LegalDefect;

import java.util.List;
import java.util.Optional;

@Repository
public interface RaportRepository extends JpaRepository<Raport, Integer> {
    List<Raport> findByForum(String forum);
    Optional<Raport> findById(Integer id);
    List<Raport> findBySubject(Subject subject);
    List<Raport> findByVindicator (Vindicator vindicator);
}
