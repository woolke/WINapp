package pl.windykacjasamochodowa.store.respositories.raportAndComments;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.windykacjasamochodowa.store.model.raportAndComments.OrderComment;

@Repository
public interface OrderCommentRepository extends JpaRepository<OrderComment, Integer> {

}
