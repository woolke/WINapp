package pl.windykacjasamochodowa.store.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.windykacjasamochodowa.store.components.StaticFunc;
import pl.windykacjasamochodowa.store.model.debt.assigment.Vindicator;
import pl.windykacjasamochodowa.store.respositories.debt.assigment.VindicatorRepository;

@Service
public class VindicatorService {

    public final VindicatorRepository vindicatorRepository;

    @Autowired
    public VindicatorService(VindicatorRepository vindicatorRepository) {
        this.vindicatorRepository = vindicatorRepository;
    }

    public  Vindicator getById(Integer id) {
        return vindicatorRepository.findById(id).orElse(null);
    }

    public  Vindicator getById(String id) {
        return vindicatorRepository.findById(StaticFunc.intFromStringID(id)).orElse(null);
    }
}
