package pl.windykacjasamochodowa.store.services;

import org.springframework.stereotype.Service;
import pl.windykacjasamochodowa.store.model.debt.order.OrderType;
import pl.windykacjasamochodowa.store.respositories.OrderTypeRepository;

@Service
public class OrderTypeService {

private final OrderTypeRepository orderTypeRepository;

    public OrderTypeService(OrderTypeRepository orderTypeRepository) {
        this.orderTypeRepository = orderTypeRepository;
    }

    public OrderType getById (Integer id) {
        return orderTypeRepository.findById(id).orElse(null);
    }

    public OrderType getByName (String name) {
        return orderTypeRepository.findByName(name).orElse(null);
    }
}
