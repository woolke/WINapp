package pl.windykacjasamochodowa.store.services;

import org.springframework.stereotype.Service;
import pl.windykacjasamochodowa.store.model._enums.debtorData.DebtorTYPE;
import pl.windykacjasamochodowa.store.model.debt.debitors.*;
import pl.windykacjasamochodowa.store.respositories.debt.debitors.SubjectRepository;

import java.util.HashSet;

@Service
public class SubjectService {
    private final SubjectRepository subjectRepository;

    public SubjectService(SubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }

    public Subject getById(String subject) {
        return subjectRepository.findById(Integer.parseInt(subject)).orElse(null);
    }

    public Subject getById(Integer subject) {
        return subjectRepository.findById(subject).orElse(null);
    }

    public Subject getByPesel(String pesel) {
        return subjectRepository.findByPesel(pesel).orElse(null);
    }

    public Subject getByNip(String nip) {
        return subjectRepository.findByNip(nip).orElse(null);
    }

    public Subject getByRegon(String regon) {
        return subjectRepository.findByRegon(regon).orElse(null);

    }

    public void save (Subject subject) {
        subjectRepository.save(subject);
    }

    public Subject addByNipOrPesel(String pesel, String nip) {
        Subject subject = subjectRepository.findByPesel(pesel)
                .orElse(subjectRepository.findByPesel(pesel)
                        .orElse(Subject.builder()
                                .nip(nip)
                                .pesel(pesel)
                                .build()));
        if (subject.getPesel() == null) {
            subject.setPesel(pesel);
        }
        if (subject.getNip() == null) {
            subject.setNip(nip);
        }
        return subject;
    }

    public Subject addByStringAndType(String fullName, String peselRegon, DebtorTYPE type) {
        if (type.equals(DebtorTYPE.FIRMA)) {
            Subject subject = getByRegon(peselRegon);
            if (subject==null) {
                subject = getEmptySet()
                    .regon(peselRegon).name(fullName)
                    .build();}
            return subject;
        } else if (type.equals(DebtorTYPE.ND)) {
            Subject subject = getByPesel(peselRegon);
            if (subject==null) {
                subject = getEmptySet()
                    .pesel(peselRegon.concat(type.getFullName())).name(fullName)
                    .build();}
            return subject;
        } else {
            Subject subject = getByPesel(peselRegon);
            if (subject==null) {
                subject = getEmptySet()
                    .pesel(peselRegon).name(fullName)
                    .build();}
            return subject;
        }

    }

    private static Subject.SubjectBuilder getEmptySet() {
        return Subject.builder()
                .adresses(new HashSet<Adress>())
                .debtors(new HashSet<Debtor>())
                .telephones(new HashSet<Telephone>())
                .emails(new HashSet<Email>())
                .debtors(new HashSet<Debtor>());
    }

}

