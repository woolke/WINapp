package pl.windykacjasamochodowa.store.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Service;
import pl.windykacjasamochodowa.store.components.StaticFunc;
import pl.windykacjasamochodowa.store.model._enums.debtorData.AdressSOURCE;
import pl.windykacjasamochodowa.store.model._enums.debtorData.AdressTYPE;
import pl.windykacjasamochodowa.store.model._enums.debtorData.AdressVERIFIED;
import pl.windykacjasamochodowa.store.model.debt.debitors.Adress;
import pl.windykacjasamochodowa.store.model.debt.debitors.Location;
import pl.windykacjasamochodowa.store.model.debt.debitors.Subject;
import pl.windykacjasamochodowa.store.respositories.debt.debitors.AdressRepository;

import java.util.Date;

@Service
public class AdressService {

    private final AdressRepository adressRepository;
    private final LocationService locationService;

    @Autowired
    public AdressService(AdressRepository adressRepository, LocationService locationService) {
        this.adressRepository = adressRepository;
        this.locationService = locationService;
    }

    public Adress buildAdress(AdressTYPE adressTYPE, AdressSOURCE adressSOURCE, String adressString) {
        return Adress.builder()
                .adressFull(adressString)
                .adressTYPE(adressTYPE)
                .adressSOURCE(adressSOURCE).build();
    }

    public Adress buildAdress(String adressString,String zipString, AdressTYPE adressTYPE, AdressSOURCE adressSOURCE) {
        return Adress.builder()
                .adressFull(adressString)
                .adressTYPE(adressTYPE)
                .adressZipCode(zipString)
                .adressSOURCE(adressSOURCE).build();
    }

    public Adress getById(String subject) {
        return adressRepository.findById(StaticFunc.intFromStringID(subject)).orElse(null);
    }

    public Adress getById(Integer subject) {
        return adressRepository.findById(subject).orElse(null);
    }

    public boolean addAdressToSubject(Subject subject, Adress adress) {

            Point adressGeoPoint = LocationService.getGeoPointFromAddress(adress.printFullAdress());
            if (adressGeoPoint==null) {
                adressGeoPoint = LocationService.getGeoPointFromAddress(adress.getAdressZipCode());
                System.out.println(adress.printFullAdress() + "nie udało się znaleść lokalizacji");
            }


        Point finalAdressGeoPoint = adressGeoPoint;
        if (subject.getAdresses().stream()
                .anyMatch(l -> l.getLocation().getId().equals(finalAdressGeoPoint))) { //todo sprawdzić porównanie dwuch punktów
            if (subject.getAdresses().stream().noneMatch(n -> n.getAdressFull().equals(adress.getAdressFull()))) {
                Location adressLocation = locationService.getByPoint(adressGeoPoint);
                adress.setToMap(false);
                adress.setAdressVERIFIED(AdressVERIFIED.ND);
                adress.setComment((new Date()).toString() + " - adres zdeaktywowany automatycznie przy dodawania z bazy -inny adres w tej samej lokalizacji");
                adress.setLocation(adressLocation);
                subject.getAdresses().add(adress);
                adress.setSubject(subject);
                return true;
            } else {
                return false;
            }
        }
        Location adressLocation = locationService.addByPoint(adressGeoPoint);
        adress.setLocation(adressLocation);
        adress.setAdressVERIFIED(AdressVERIFIED.NIEZWERYFIKOWANY);
        adress.setComment((new Date()).toString() + " - import");
        adress.setToMap(true);
        subject.getAdresses().add(adress);
        adress.setSubject(subject);
        return true;
    }


}
