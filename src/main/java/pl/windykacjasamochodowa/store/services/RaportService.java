package pl.windykacjasamochodowa.store.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.windykacjasamochodowa.store.components.CsvParse;
import pl.windykacjasamochodowa.store.components.StaticFunc;
import pl.windykacjasamochodowa.store.model._enums.raport.RaportAMOUNT;
import pl.windykacjasamochodowa.store.model._enums.raport.RaportSTATUS;
import pl.windykacjasamochodowa.store.model._enums.raport.RaportTYPE;
import pl.windykacjasamochodowa.store.model.debt.assigment.Assigment;
import pl.windykacjasamochodowa.store.model.debt.assigment.Vindicator;
import pl.windykacjasamochodowa.store.model.debt.debitors.Debtor;
import pl.windykacjasamochodowa.store.model.debt.debitors.Subject;
import pl.windykacjasamochodowa.store.model.debt.order.Order;
import pl.windykacjasamochodowa.store.model.raportAndComments.Raport;
import pl.windykacjasamochodowa.store.respositories.raportAndComments.RaportRepository;

import java.awt.*;
import java.io.InputStream;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RaportService {
    private final RaportRepository raportRepository;
    private final VindicatorServiceImpl userDetailsService;
    private final OrderService orderService;
    private final VindicatorService vindicatorService;


    @Autowired
    public RaportService(RaportRepository raportRepository, VindicatorServiceImpl userDetailsService, OrderService orderService, VindicatorService vindicatorService) {
        this.raportRepository = raportRepository;
        this.userDetailsService = userDetailsService;
        this.orderService = orderService;
        this.vindicatorService = vindicatorService;
    }



    public List<Raport> getList() {
        if (userDetailsService.getLoggedInUser().isManager()) {
            return getAll();
        }
        return getWtAll(userDetailsService.getLoggedInUser().getId());
    }

    public List<Raport> getAll() {
        return raportRepository.findAll();
    }

    public List<Raport> getWtAll(Integer wtID) {
        List<Raport> wtAssigment = raportRepository.findAll();
        return wtAssigment = wtAssigment.stream()
                .filter(assigment -> assigment.getVindicator().getId().equals(wtID))
                .collect(Collectors.toList());
    }

    public Raport getById(Integer id) {
        Optional<Raport> byId = raportRepository.findById(id);
        return byId.get();
    }


    public static List<Raport> getByLocation(List<Raport> raportList, Point location) { //todo zapytanieJPA
        List<Raport> byAdress = raportList.stream().filter(a -> a.getAdress().getLocation().equals(location))
                .collect(Collectors.toList());
        return byAdress;
    }

    public static List<Raport> getByForum(List<Raport> raportList, String forum) {
        List<Raport> byForum = raportList.stream().filter(a -> a.getForum().equals(forum))
                .collect(Collectors.toList());
        return byForum;
    }


    public List<Raport> getByForum(String forum) {
       return raportRepository.findByForum(forum);
    }

    public List<Raport> getBySubject(Subject subject) {
        return raportRepository.findBySubject(subject);
    }


    public List<Raport> getByAllSubject(Assigment assigment) {
        ArrayList<Raport> raportArrayList =  new ArrayList<>();
        for (Debtor debtor : assigment.getOrder().getDebtors()) {
            List<Raport> bySubject = raportRepository.findBySubject(debtor.getSubject());
            raportArrayList.addAll(bySubject);
        }

        return raportArrayList;
    }


    public Raport save(Raport raport) {
        return raportRepository.save(raport);
    }

    public void saveAll (List<Raport> raportList) {
        raportRepository.saveAll(raportList);
    }


    public Map<String, String> saveRaportFromArchive (InputStream csvInuptStream){
        ArrayList<Map<String, String>> mapFromKiArchiveCsv = CsvParse.getMapRaportFromArchiveCsv(csvInuptStream);
        HashMap<String, String> unsaveRaport= new HashMap<>();
        for (Map<String, String> csvRow : mapFromKiArchiveCsv) {

            try {
                Raport raportRow= addRaportArchiveSingleRow(csvRow);
                saveForumRaportToAllDebtors(raportRow);

            } catch (Exception e) {
                unsaveRaport.put(csvRow.get("forum"),e.getMessage());
                System.out.println(" Nie utworzono zlecenia: "
                        + csvRow.get("forum") +", " + e.getMessage());

            }


        }

        return (unsaveRaport);

    }

    private Raport addRaportArchiveSingleRow(Map<String, String> csvRow) {
        Vindicator wT = vindicatorService.getById
                (csvRow.get("idWt"));
        String forum = (csvRow.get("forum"));
        Date raportDate = StaticFunc.getDateFromStringRRRR_MM_DD(csvRow.get("date"));
        RaportTYPE raportTYPE = RaportTYPE.valueOf(csvRow.get("type"));
        String comment = csvRow.get("comment")+" | " + csvRow.get("car") + " | " +csvRow.get("ustalenie");
        Raport buildRaport = Raport.builder().raportTimeStamp(new Date())
                .comment(comment)
                .forum(forum)
                .vindicator(wT)
                .raportTYPE(raportTYPE)
                .raportDate(raportDate)
                .raportAMOUNT(RaportAMOUNT.ND)
                .raportSTATUS(RaportSTATUS.ND)
                .raportSTATUS2("z Archiwum")
                .hideComment("")
                .raportAMOUNT(RaportAMOUNT.ND)
                .build();

        return buildRaport;
    }

    public void saveForumRaportToAllDebtors(Raport raportRow) {
        Order byIdOrBlank = orderService.getByIdOrBlank(raportRow.getForum());
        if (byIdOrBlank.getDebtors().size()==0) {
            raportRepository.save(raportRow);
            System.out.println(raportRow.getForum().concat(" nie dodano raportu do żadnego dłużnika"));
        } else {
            for (Debtor debtor : byIdOrBlank.getDebtors()) {
                Raport raportToSave = raportRow;
                raportToSave.setSubject(debtor.getSubject());
                raportRepository.save(raportToSave);
                System.out.println(raportToSave.getForum().concat(" dodano raport do: ")
                        .concat(raportToSave.getSubject().printName()));
            }
        }
    }
}
