package pl.windykacjasamochodowa.store.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.windykacjasamochodowa.store.model.debt.assigment.Vindicator;
import pl.windykacjasamochodowa.store.respositories.RolesRepository;
import pl.windykacjasamochodowa.store.respositories.debt.assigment.VindicatorRepository;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class VindicatorServiceImpl implements UserDetailsService {
    private static final Logger LOG = LoggerFactory.getLogger(VindicatorServiceImpl.class);

    @Autowired
    private VindicatorRepository vindicatorRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private RolesRepository rolesRepository;


    @Override
    @Transactional()
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Vindicator vindicator = vindicatorRepository.findByUsername(username);
        if (vindicator == null) {
            throw new UsernameNotFoundException("Username is not in database");
        }

        Set<GrantedAuthority> grantedAuthorities =
                vindicator.getRoles().stream()
                        .map(r -> new SimpleGrantedAuthority("ROLE_" + r.getName()))
                        .collect(Collectors.toSet());

        return new org.springframework.security.core.userdetails.User(
                vindicator.getUsername(), vindicator.getPassword(), grantedAuthorities);
    }

    public void save(Vindicator vindicator) {
        vindicator.setPassword(bCryptPasswordEncoder.encode(vindicator.getPassword()));
        vindicator.setRoles(new HashSet(rolesRepository.findAll()));
        vindicatorRepository.save(vindicator);
    }

    public Iterable getAllUsersNamesAndTheirRoles() {
        return vindicatorRepository.findAll().stream().map(user -> {
            Vindicator tmpUser = new Vindicator();
            tmpUser.setUsername(user.getUsername());
            tmpUser.setRoles(user.getRoles());
            return tmpUser;
        }).collect(Collectors.toList());
    }


    public Vindicator getLoggedInUser() {
        Optional<Vindicator> user = null;
        try {
            org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            user = Optional.ofNullable(vindicatorRepository.findByUsername(principal.getUsername()));
            return user.get();
        } catch (Exception e) {
            return null;
        }
    }



}
