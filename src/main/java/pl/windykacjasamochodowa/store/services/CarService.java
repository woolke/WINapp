package pl.windykacjasamochodowa.store.services;

import org.springframework.stereotype.Service;
import pl.windykacjasamochodowa.store.model.debt.order.Car;
import pl.windykacjasamochodowa.store.respositories.debt.order.CarRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CarService {
    private final CarRepository carRepository;

    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public Car getByNrRej(String nrRej) {
        return carRepository.findById(nrRej).orElse(null);
    }
}
