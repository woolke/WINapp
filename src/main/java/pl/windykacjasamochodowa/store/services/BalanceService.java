package pl.windykacjasamochodowa.store.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.windykacjasamochodowa.store.model.debt.order.Balance;
import pl.windykacjasamochodowa.store.respositories.debt.order.BalanceRepository;

import java.util.List;

@Service
public class BalanceService {

    public final BalanceRepository balanceRepository;

    @Autowired
    public BalanceService(BalanceRepository balanceRepository) {
        this.balanceRepository = balanceRepository;
    }

    public List<Balance> getOrderBalances(String forum) {
        return balanceRepository.findByOrder_Forum(forum);
    }


}
