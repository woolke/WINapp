package pl.windykacjasamochodowa.store.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.windykacjasamochodowa.store.model.debt.debitors.*;
import pl.windykacjasamochodowa.store.model.debt.order.Balance;
import pl.windykacjasamochodowa.store.model.debt.order.Order;
import pl.windykacjasamochodowa.store.model.debt.order.OrderDetails;
import pl.windykacjasamochodowa.store.model.debt.order.Payment;
import pl.windykacjasamochodowa.store.respositories.debt.order.OrderRepository;

import java.util.HashSet;

@Service
public class OrderService {

    private final OrderRepository orderRepository;

    @Autowired
    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public Order getByIdOrBlank (String forum) {
       return orderRepository.findById(forum).orElse(getBlank(forum));
    }

    public Order getBlank(String forum) {
                return getEmptySet().forum(forum).build();
    }

    private static Order.OrderBuilder getEmptySet() {
        return Order.builder()
                .balances(new HashSet<Balance>())
                .debtors(new HashSet<Debtor>())
                .orderDetails(new HashSet<OrderDetails>())
                .payments(new HashSet<Payment>())
;
    }

    public static String printIBAN (String iban) {
        if (iban.length() == 26) {
            return iban.substring(0,2) + " " + iban.substring(2,6)+ " " + iban.substring(6,10)+ " " + iban.substring(10, 14)
                    + " " + iban.substring(14,18)+ " " + iban.substring(18,22)+ " " + iban.substring(22,26);
        } else {
            return iban;
        }
    }

}
