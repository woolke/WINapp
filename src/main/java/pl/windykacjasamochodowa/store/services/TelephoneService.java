package pl.windykacjasamochodowa.store.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.windykacjasamochodowa.store.model._enums.debtorData.TelephoneSOURCE;
import pl.windykacjasamochodowa.store.model._enums.debtorData.TelephoneTYPE;
import pl.windykacjasamochodowa.store.model.debt.debitors.Subject;
import pl.windykacjasamochodowa.store.model.debt.debitors.Telephone;
import pl.windykacjasamochodowa.store.respositories.debt.debitors.TelephoneRepository;

import java.util.HashSet;
import java.util.Optional;

@Service
public class TelephoneService {
    private final TelephoneRepository telephoneRepository;

    @Autowired
    public TelephoneService(TelephoneRepository telephoneRepository) {
        this.telephoneRepository = telephoneRepository;
    }

    public Telephone getById (Integer id) {
        return telephoneRepository.findById(id).orElse(null);
    }

    public Telephone getByNr(String telephoneNr) {
        return telephoneRepository.findByTelephone(telephoneNr).orElse(null);
    }

    public Telephone buildFromNrAndTypeSource(String tel, TelephoneSOURCE telephoneSOURCE, TelephoneTYPE telephoneTYPE) {
        return Telephone.builder().telephone(tel.replace(" ", ""))
                .telephoneTYPE(telephoneTYPE).telephoneSOURCE(telephoneSOURCE).build();
    }

    public void addTelToSubject(Subject subject, Telephone telephone) {
        if (subject.getTelephones().stream()
                .noneMatch(t -> t.getTelephone().equals(telephone.getTelephone()))) {

            telephone.setSubject(subject);
            Telephone savedTelephone = telephoneRepository.save(telephone);
            subject.getTelephones().add(savedTelephone);

        }
    }

    public static String prettyPrintTelephone(Telephone telephone) {
        String telString = telephone.getTelephone();
        String printString = "";
        if (telString.substring(0, 1).equals("+")) {
            if (telString.substring(0, 3).equals("+48")) {
                if (isMobilePhone(telString)) {
                    printString = prettyMobile(telString);
                } else {
                    printString = prettyHomePhone(telString);
                }
            }

        } else if (telString.length() == 9) {
            if (isMobilePhone("+48"+telString)) {
                printString = prettyMobile("+48" + telString);
            } else {
                printString = prettyHomePhone("+48" + telString);
            }


        } else {
            printString = telString;
        }


        return printString;
    }

    private static String prettyHomePhone(String telString) {
        String printString;
        printString = telString.substring(0, 3) + " ("
                + telString.substring(3, 5) + ") " + telString.substring(5,8)+ "-" + telString.substring(8,10)
        +"-" + telString.substring(10);
        return printString;
    }

    private static String prettyMobile(String telString) {
        String printString;
        printString = telString.substring(0, 3) + " " + telString.substring(3,6) + "-" + telString.substring(6,9)
        +"-" + telString.substring(9);
        return printString;
    }

    private static boolean isMobilePhone(String tel) {
        HashSet<Integer> areaCodes = new HashSet<>();
        areaCodes.add(12);
        areaCodes.add(13);
        areaCodes.add(14);
        areaCodes.add(15);
        areaCodes.add(16);
        areaCodes.add(17);
        areaCodes.add(18);
        areaCodes.add(22);
        areaCodes.add(23);
        areaCodes.add(24);
        areaCodes.add(25);
        areaCodes.add(29);
        areaCodes.add(32);
        areaCodes.add(33);
        areaCodes.add(34);
        areaCodes.add(41);
        areaCodes.add(42);
        areaCodes.add(43);
        areaCodes.add(44);
        areaCodes.add(46);
        areaCodes.add(48);
        areaCodes.add(52);
        areaCodes.add(54);
        areaCodes.add(55);
        areaCodes.add(56);
        areaCodes.add(58);
        areaCodes.add(59);
        areaCodes.add(61);
        areaCodes.add(62);
        areaCodes.add(63);
        areaCodes.add(65);
        areaCodes.add(67);
        areaCodes.add(68);
        areaCodes.add(71);
        areaCodes.add(74);
        areaCodes.add(75);
        areaCodes.add(76);
        areaCodes.add(77);
        areaCodes.add(81);
        areaCodes.add(82);
        areaCodes.add(83);
        areaCodes.add(84);
        areaCodes.add(85);
        areaCodes.add(86);
        areaCodes.add(87);
        areaCodes.add(89);
        areaCodes.add(91);
        areaCodes.add(94);
        areaCodes.add(95);
        return !areaCodes.contains(Integer.parseInt(tel.substring(3, 5)));
    }

}
