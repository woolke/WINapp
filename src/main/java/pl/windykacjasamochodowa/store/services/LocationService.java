package pl.windykacjasamochodowa.store.services;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.windykacjasamochodowa.store.model.debt.debitors.Location;
import pl.windykacjasamochodowa.store.respositories.debt.debitors.LocationRepository;
import org.springframework.data.geo.Point;
import java.awt.*;
import java.net.URL;
import java.util.Scanner;

@Service
public class LocationService {

    private final LocationRepository locationRepository;

    @Autowired
    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }
    public Location getByPoint(Point point) {
        return locationRepository.findById(point).orElse(null);
    }

    public Location addByPoint ( Point point) {
        Location build = Location.builder().id(point).lat(point.getX()).lng(point.getY()).build();
        return locationRepository.save(build);
    }

    public static Point getGeoPointFromAddress(String locationAddress) {
        Point locationPoint = null;
        String locationAddres = locationAddress.replaceAll(" ", "%20");
        String str = "https://maps.googleapis.com/maps/api/geocode/json?&key=AIzaSyAPwxs46c_fWWT7FGW9qov9HcLSB29RGLY&address="
                + locationAddres + "&sensor=true";

        String ss = readWebService(str);
        JSONObject json;
        try {

            String lat, lon;
            json = new JSONObject(ss);
            JSONObject geoMetryObject = new JSONObject();
            JSONObject locations = new JSONObject();
            JSONArray jarr = json.getJSONArray("results");

            json = jarr.getJSONObject(0);
            geoMetryObject = json.getJSONObject("geometry");
            locations = geoMetryObject.getJSONObject("location");
            lat = locations.getString("lat");
            lon = locations.getString("lng");

            locationPoint = new Point(Double.parseDouble(lat),
                    (Double.parseDouble(lon)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return locationPoint;
    }

    private static String readWebService(String s) {
        try {
            String str = "";
            URL url = new URL(s);
            Scanner sc = new Scanner(url.openConnection().getInputStream());
            while (sc.hasNext()) {
                str = str + sc.nextLine();
            }

            return str;
        } catch (Exception e) {
            return null;
        }
    }
}
