package pl.windykacjasamochodowa.store.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.windykacjasamochodowa.store.model.debt.assigment.Assigment;
import pl.windykacjasamochodowa.store.respositories.debt.assigment.AssigmentRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class AssigmentService {
    private final AssigmentRepository assigmentRepository;
    private final VindicatorServiceImpl vindicatorServiceImpl;


    @Autowired
    public AssigmentService(AssigmentRepository assigmentRepository, VindicatorServiceImpl vindicatorServiceImpl) {
        this.assigmentRepository = assigmentRepository;
        this.vindicatorServiceImpl = vindicatorServiceImpl;
    }

    public void save(Assigment assigment) {
        assigmentRepository.save(assigment);
    }

    public void saveAll(List<Assigment> assigmentList) {
        assigmentRepository.saveAll(assigmentList);
    }

    public Assigment getById(Integer id) {
        Optional<Assigment> byId = assigmentRepository.findById(id);
        return byId.orElse(null); //todo nullEx
    }

    public Assigment getByForumAndCampaign (String forum, String campaign) {
        Optional<Assigment> byForumAndCampaignName = assigmentRepository
                .findByCampaignNameAndOrderForum(campaign, forum);
        return byForumAndCampaignName.orElse(Assigment.builder().build()); //todo nullEx
    }

    public List<Assigment> getList() {
        if (vindicatorServiceImpl.getLoggedInUser().isManager()) {
            return getActualListAll();
        }
        return getWtActualList(vindicatorServiceImpl.getLoggedInUser().getId());
    }

//    public static Assigment getByForum(List<Assigment> assigmentList, String forum) {
//        Optional<Assigment> byId = assigmentList.stream().filter(a -> a.getOrder().getForum().equals(forum)).findFirst();
//        return byId.orElse(null); //todo nullEX
//    }

    public Assigment getByForum(String forum) {
        return assigmentRepository.findTopByOrder_Forum(forum)
                .orElse(Assigment.builder().build());
    }

    public List<Assigment> getAll () {
        return assigmentRepository.findAll();
    }

    private List<Assigment> getActualListAll() {
        return assigmentRepository.findByCampaignCampaignEnd_GreaterThan(new Date());
    }

    private List<Assigment> getWtActualList(Integer wtID) {
        return assigmentRepository.findByVindicator_IdAndCampaign_CampaignEndGreaterThan(wtID, new Date());
    }



}
