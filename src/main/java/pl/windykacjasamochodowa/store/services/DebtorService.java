package pl.windykacjasamochodowa.store.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.windykacjasamochodowa.store.model._enums.debtorData.DebtorROLE;
import pl.windykacjasamochodowa.store.model._enums.debtorData.DebtorTYPE;
import pl.windykacjasamochodowa.store.model.debt.debitors.Debtor;
import pl.windykacjasamochodowa.store.model.debt.debitors.Subject;
import pl.windykacjasamochodowa.store.model.debt.order.Order;
import pl.windykacjasamochodowa.store.respositories.debt.debitors.DebtorRepository;

import java.util.HashSet;
import java.util.Optional;

@Service
public class DebtorService {

    private final DebtorRepository debtorRepository;

    @Autowired
    public DebtorService(DebtorRepository debtorRepository) {
        this.debtorRepository = debtorRepository;
    }

    public Debtor addSubjectToForum(Subject subject, DebtorTYPE type, DebtorROLE role) {
        Optional<Debtor> bySubject = debtorRepository.findBySubject(subject);
        Debtor debtor = bySubject.orElse(buildDebtor(subject, type, role));
        return debtor;
    }

    private Debtor.DebtorBuilder getBlank() {
        return Debtor.builder().orders(new HashSet<Order>());
    }

    private Debtor buildDebtor(Subject subject, DebtorTYPE type, DebtorROLE role) {
        return Debtor.builder()
                .orders(new HashSet<Order>())
                .debtorROLE(role)
                .debtorTYPE(type)
                .subject(subject).build();
    }
}
