package pl.windykacjasamochodowa.store.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.windykacjasamochodowa.store.components.StaticFunc;
import pl.windykacjasamochodowa.store.model.debt.assigment.Assigment;
import pl.windykacjasamochodowa.store.model.debt.assigment.Campaign;
import pl.windykacjasamochodowa.store.respositories.debt.assigment.CampaignRepository;

import java.util.Date;
import java.util.Optional;

import static pl.windykacjasamochodowa.store.components.StaticFunc.getDateFromStringRRRR_MM_DD;

@Service
public class CampaignService {

    private final CampaignRepository campaignRepository;

    @Autowired
    public CampaignService(CampaignRepository campaignRepository) {
        this.campaignRepository = campaignRepository;
    }




    public Campaign getOrAdd(String name, String campaignStart, String campaignEnd) {
        Optional<Campaign> byName = campaignRepository.findByName(name);
        return byName.orElseGet(() -> add(name, campaignStart, campaignEnd));
    }

    public Campaign add(String name, String campaignStart, String campaignEnd) {
        Campaign campaign = Campaign.builder().campaignEnd(getDateFromStringRRRR_MM_DD(campaignEnd))
                .campaignStart(getDateFromStringRRRR_MM_DD(campaignStart))
                .name(name).build();
        return campaignRepository.save(campaign);
    }

    public Campaign getByName(String name) {
        return campaignRepository.findById(name).orElse(null);
    }

    public Campaign addToAssiment (Assigment assigment, String campaignStart, String campaignEnd) {
        Optional<Campaign> campaignByName= campaignRepository.findByName(assigment.getCampaign().getName());
        if (campaignByName.isPresent()) {

        }
        return null; //todo Sprawdzić czy daty takie same
    }


}
