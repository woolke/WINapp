package pl.windykacjasamochodowa.store;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/** 
 * Główna klasa aplikacji. Ona rozpoczyna start kontekstu springa. 
 * Adnotaccja @SpringBootApplication jednocześnie sprawia, że klasa ta jest
 * ekwiwalentem @Configuration. Tylko jedna klasa w aplikacji może mieć adnotację
 * @SpringBootApplication w przeciwieństwie do @Configuration - tych może być więcej.
 */
@SpringBootApplication
public class WinAplication {

    public static void main(String[] args) {
        SpringApplication.run(WinAplication.class, args);
    }


}
