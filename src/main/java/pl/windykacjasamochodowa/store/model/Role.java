package pl.windykacjasamochodowa.store.model;

import lombok.*;
import pl.windykacjasamochodowa.store.model.debt.assigment.Vindicator;

import javax.persistence.*;
import java.util.Set;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "roles")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String name;
    @ManyToMany(mappedBy = "roles")
    private Set<Vindicator> vindicators;

}
