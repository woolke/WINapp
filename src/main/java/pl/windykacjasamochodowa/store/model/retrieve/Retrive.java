package pl.windykacjasamochodowa.store.model.retrieve;

import lombok.*;
import org.hibernate.validator.constraints.UniqueElements;
import pl.windykacjasamochodowa.store.model.debt.assigment.Vindicator;
import pl.windykacjasamochodowa.store.model.debt.order.Order;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "retrives")
public class Retrive {
    @Id
    String nrRej;

    @Temporal(TemporalType.DATE)
    Date retriveDate;

    @ManyToOne
    @JoinColumn(name = "forum")
    Order order;

    @OneToMany(mappedBy = "retrive")
    Set<LegalDefect> legalDefects;

    @OneToMany(mappedBy = "retrive")
    Set<Policy> policies;

    @JoinColumn(name = "idWT")
    @ManyToOne
    Vindicator vindicator;

//    @Enumerated(EnumType.STRING)
//    RetriveDocStatus retriveDocStatus = getRetriveDocStatus();
//
//    @Enumerated
//    RetriveStatusPPR holStatus;
//
//    @Enumerated
//    RetriveStatusPPR pprStatus;
//
//    @Enumerated
//    RetriveKP kp;
//
//    @Enumerated
//    RetriveDR dr;
//
//    @Enumerated
//    RetriveLegalSTATUS legalSTATUS;


    String parking;
    String vin;
    String modelAndBrand;

    @UniqueElements
    Integer idPpr;

    String mileage;
    String retrivingPerson;
    String comments;
    String settlementComments;


    @Temporal(TemporalType.DATE)
    Date WT_SettlementDate;

    @Temporal(TemporalType.DATE)
    Date WIN_SettlementDate;


}
