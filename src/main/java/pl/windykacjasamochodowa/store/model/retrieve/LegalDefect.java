package pl.windykacjasamochodowa.store.model.retrieve;

import lombok.*;

import javax.persistence.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "legal_defect")
public class LegalDefect {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
     Integer id;

    String bailiffName;
    String defectSignature;
    String comment;

    @ManyToOne
    @JoinColumn(name = "nrRej")
    Retrive retrive;

}
