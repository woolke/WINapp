package pl.windykacjasamochodowa.store.model.raportAndComments;

import lombok.*;
import pl.windykacjasamochodowa.store.model.debt.order.Order;

import javax.persistence.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table (name = "order_comments")
public class OrderComment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String comment;

    @ManyToOne
    @JoinColumn (name = "forum")
    Order order;
}

