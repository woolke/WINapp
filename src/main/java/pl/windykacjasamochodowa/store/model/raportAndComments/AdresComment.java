package pl.windykacjasamochodowa.store.model.raportAndComments;

import lombok.*;
import pl.windykacjasamochodowa.store.model.debt.debitors.Adress;

import javax.persistence.*;


@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table (name = "adres_comments")
public class AdresComment {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    Integer id;
    String comment;

    @ManyToOne
    Adress adress;
}
