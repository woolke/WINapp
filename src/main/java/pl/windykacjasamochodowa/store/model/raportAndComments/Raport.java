package pl.windykacjasamochodowa.store.model.raportAndComments;

import lombok.*;
import pl.windykacjasamochodowa.store.dtos.RaportPrintDto;
import pl.windykacjasamochodowa.store.model._enums.raport.*;
import pl.windykacjasamochodowa.store.model.debt.assigment.Vindicator;
import pl.windykacjasamochodowa.store.model.debt.debitors.Adress;
import pl.windykacjasamochodowa.store.model.debt.debitors.Email;
import pl.windykacjasamochodowa.store.model.debt.debitors.Subject;
import pl.windykacjasamochodowa.store.model.debt.debitors.Telephone;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "raports")
public class Raport {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    String forum;

    @Temporal(TemporalType.TIMESTAMP)
    Date raportDate;

    @Temporal(TemporalType.TIMESTAMP)
    Date raportTimeStamp;

    @Temporal(TemporalType.DATE)
    Date declarationRetrieveDate;

    @Temporal(TemporalType.DATE)
    Date declarationPaymantDate;

    @Temporal(TemporalType.DATE)
    Date scheduledTask;

    @ManyToOne()
    @JoinColumn(name = "wt_id")
    Vindicator vindicator;

    String car;
    String mortgage;

    @ManyToOne
    @JoinColumn(name = "subject_id")
    Subject subject;

    @ManyToOne
    @JoinColumn(name = "adress_id")
    Adress adress;

    @ManyToOne
    @JoinColumn(name = "telephone_id")
    Telephone telephone;

    @ManyToOne
    @JoinColumn(name = "email")
    Email email;


    @Enumerated(EnumType.STRING)
    RaportTYPE raportTYPE = getRaportTYPE();

    @Enumerated(EnumType.STRING)
    RaportTELEPHONE raportTELEPHONE = getRaportTELEPHONE();

    @Enumerated(EnumType.STRING)
    RaportADRESS raportADRESS = this.getRaportADRESS();

    @Enumerated(EnumType.STRING)
    RaportAMOUNT raportAMOUNT = getRaportAMOUNT();

    @Enumerated(EnumType.STRING)
    RaportCAR raportCAR = getRaportCAR();

    @Enumerated(EnumType.STRING)
    RaportSTATUS raportSTATUS = getRaportSTATUS();

    String raportSTATUS2;
    String toString;
    BigDecimal declarationPaymantValue;
    Boolean priority;
    String comment;
    String hideComment;


    public RaportPrintDto getRaportPrintDtoFromRaport() {
        RaportPrintDto raportPrintDto = RaportPrintDto.builder()
                .forum(this.forum)
                .subject(this.subject.getName())
                .vindicator(this.vindicator.getId().toString())
                .raportDate(this.raportDate.toString())
                .declarationPaymantDate(this.declarationPaymantDate.toString())
                .scheduledTask(this.scheduledTask.toString())
                .adress(this.adress.getAdressFull())
                .telephone(this.telephone.getTelephone())
                .declarationPaymantValue(this.declarationPaymantValue.toString())
                .raportTYPE(this.raportTYPE.getFullName())
                .raportADRESS(this.raportADRESS.getFullName())
                .raportAMOUNT(this.raportAMOUNT.getFullName())
                .raportSTATUS(this.raportSTATUS.getFullName())
                .raportSTATUS2(this.raportSTATUS2)
                .comment(this.comment)
                .hideComment(this.hideComment)
                .car(this.car)
                .raportCAR(this.raportCAR.getFullName())
                .declarationRetrieveDate(this.declarationRetrieveDate.toString()).build();
        return raportPrintDto;


    }

    public String fullPrintString() {
        String print = "WT" + vindicator.getId() + " " + raportDate.toString() + " - "
                + forum + ": "
                + subject.printName() + " ("
                + raportTYPE.getFullName() + ", " + raportSTATUS.getFullName() + ", " + raportSTATUS2 + ")\n";

        if (adress!=null) {
            print = print.concat(raportADRESS.getFullName() +" (" + adress.printFullAdress() +")\n");
        }

        if (telephone!=null) {
            print = print.concat(raportTELEPHONE.getFullName() +" (" + telephone.getTelephone() +")\n");
        }

        print = print.concat(raportAMOUNT.getFullName());

        if (declarationPaymantDate!=null) {
            print = print.concat("(" +declarationPaymantDate.toString() + declarationPaymantValue +")\n");
        }  else {
            print = print.concat("\n");
        }

        if (car!=null) {
            print = print.concat("(" +getRaportCAR().toString());
        }  else {
            print = print.concat("\n");
        }
        print = print.concat(comment +"; " + hideComment);

        return print;

    }


}


