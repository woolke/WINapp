package pl.windykacjasamochodowa.store.model._enums.raport;

import lombok.Getter;

@Getter
public enum RaportSTATUS {
    ND ("-"),
    PRZYWLASZCZENIE ("Zgłoszono przywłaszczenie"),
    NOTATKA_KARNA ("Sporzadzono notatkę karną"),
    ODMOWA_SPLATY ("ODMOWA_SPŁATY"),
    BRAK_DEKLARACJI_SPLATY ("BRAK_DEKLARACJI_SPŁATY"),
    DEKLARACJA_SPLATY ("DEKLARACJA_SPŁATY"),
    WNIOSEK_O_UGODE ("WNIOSEK_O_UGODĘ"),
    ZGON_KREDYTOBIORCY ("ZGON_KREDYTOBIORCY");


    private final String fullName;

    RaportSTATUS(String fullName) {
        this.fullName = fullName;
    }
}
