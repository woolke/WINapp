package pl.windykacjasamochodowa.store.model._enums.raport;

import lombok.Getter;

@Getter
public enum RaportCAR {
    ND ("-"),
    NIE_WIDZIANO_AUTA ("Nie widziano auta"),
    PRZEJECIE_POJAZDU ("Przejęcie pojazdu"),
    DEKLARACJA_WYDANIA ("Deklaracja wydania pojazdu"),
    AUTO_PORZUCONE ("Auto wygląda na porzucone"),
    AUTO_UZYTKOWANE ("Auto wygląda na użytkowane"),
    ODMOWA ("Kategoryczna odmowa wydania pojazdu"),
    ODMOWA_DOZOR ("Odmowa z uwagi na sprawowany dozór"),
    SZKODA_CALKOWITA("Szkoda całkowita"),
    KRADZIEZ ("Kradzież"),
    POJAZD_SPRZEDANY_WP ("Pojazd przejęty przez w. pierwotnego"),
    POJAZD_SPRZEDANY_KOMORNIK ("Pojazd sprzedany przez komornika"),
    UDZIAL_SPRZEDANY_KOMORNIK ("Udział sprzedany przez komornika"),
    SPRZEDAZ_BEZ_ZGODY("Sprzedaż pojazdu bez zgody");

    private final String fullName;

    RaportCAR(String fullName) {
        this.fullName = fullName;
    }
}
