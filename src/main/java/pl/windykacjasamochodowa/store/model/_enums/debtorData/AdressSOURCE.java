package pl.windykacjasamochodowa.store.model._enums.debtorData;

import lombok.Getter;

@Getter
public enum AdressSOURCE {
    ND ("-"),
    OD_KB ("Od kredytobiorcy"),
    OD_RODZINY ("Od rodziny"),
    OD_OSOBY_3 ("od osoby trzeciej"),
    CEIDG_KRS ("Z baz CEIDG, KRS"),
    USTALONE_SKIP ("Skip tracing"),
    ZE_ZLECENIA ("Ze zlecenia"),
    Z_DOKUMENTACJI_RECZNIE ("Z dokumentacji kredytowej ręcznie");


    private final String fullName;

    AdressSOURCE(String name) {
        this.fullName= name;
    }
    }
