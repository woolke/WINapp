package pl.windykacjasamochodowa.store.model._enums.debtorData;

import lombok.Getter;

@Getter
public enum DebtorROLE {
    ND ("-", "-"),
    KB ("Kredytobiorca", "KB"),
    WP("Współkredytobiorca", "WP"),
    P("Poręczyciel", "P"),
    W("Właściciel zabezpieczenia", "W");

    private final String fullName;
    private final String shortName;


    DebtorROLE(String fullName, String shortName) {
        this.fullName = fullName;
        this.shortName = shortName;
    }
}
