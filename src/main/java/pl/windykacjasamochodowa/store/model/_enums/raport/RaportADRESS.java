package pl.windykacjasamochodowa.store.model._enums.raport;

import lombok.Getter;

@Getter
public enum RaportADRESS {
    ND ("-"),
    UNIKA_KONTAKTU ("UNIKA_KONTAKTU"),
    NIEOBECNOŚĆ_DŁUGOTRWAŁA ("NIEOBECNOŚĆ_DŁUGOTRWAŁA"),
    NIEZASTANO_NIEOBECNOŚĆ_CZASOWA ("NIEZASTANO_NIEOBECNOŚĆ_CZASOWA"),
    WSKAZANY_ADRES_JEST_NIEAKTUALNY ("WSKAZANY_ADRES_JEST_NIEAKTUALNY");


    private final String fullName;



    RaportADRESS(String fullName) {
        this.fullName = fullName;
    }
}



