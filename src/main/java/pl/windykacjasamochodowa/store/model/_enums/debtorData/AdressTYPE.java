package pl.windykacjasamochodowa.store.model._enums.debtorData;

import lombok.Getter;


@Getter
public enum AdressTYPE {
    ND ("-"),
    GLOWNY ("Główny"),
    ZAMELDOWANIA ("Zameldowania"),
    KORESPONDENCYJNY ("Korespondencyjny"),
    RODZINA ("Rodzina (ma kontakt)"),
    RODZINA_NIE ("Rodzina (nie ma kontaktu)"),
    PRACA ("Praca"),
    INNY ("Inny");


    private final String fullName;

    AdressTYPE(String name) {
        this.fullName = name;
    }

}
