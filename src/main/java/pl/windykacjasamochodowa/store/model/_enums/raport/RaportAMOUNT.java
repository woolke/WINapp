package pl.windykacjasamochodowa.store.model._enums.raport;

import lombok.Getter;

@Getter
public enum RaportAMOUNT {
    ND ("-"),
    BRAK_INFORMACJI ("Brak informacji"),
    SPLATA_CALOSCI ("Spłata całości"),
    SPLATA_CZESCI_Z_UMORZENIEM ("Spłata jednorazowa z umorzeniem"),
    UGODA_Z_WIEKSZA_WPLATA ("Ugoda z większą wpłatą"),
    UGODA_Z_WIEKSZA_WPLATA_I_UMORZENIEM ("Ugoda z większą wpłatą i umorzeniem "),
    UGODA_BEZ_WIEKSZEJ_WPLATY ("Ugoda bez większej wpłaty"),
    UGODA_BEZ_WIEKSZEJ_WPLATY_Z_UMORZENIEM ("Ugoda z umorzeniem"),
    TYMCZASOWY_BRAK_MOZLIWOSCI("Tymczasowy brak możliwości spłaty"),
    DLUGOTRWALY_BRAK_MOZLIWOSCI("Brak możliwości spłaty w dłuższym okresie"),
    BRAK_CHECI ("Brak chęci spłaty");

    private final String fullName;


    RaportAMOUNT(String fullName) {
        this.fullName = fullName;
    }
}
