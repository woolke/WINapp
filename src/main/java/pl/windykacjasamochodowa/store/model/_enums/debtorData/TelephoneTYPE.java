package pl.windykacjasamochodowa.store.model._enums.debtorData;

import lombok.Getter;


@Getter
public enum TelephoneTYPE {
    ND ("-"),
    KOMORKOWY ("Komórkowy"),
    DOMOWY ("Domowy"),
    RODZINA ("Rodzina"),
    RODZINA_NIE ("Rodzina (nie ma kontaktu)"),
    OSOBA_3 ("Osoba trzecia (ma kontakt)"),
    OSOBA_3_NIE ("Osoba trzecia"),
    PRACA ("Praca"),
    INNY ("Inny");


    private final String fullName;

    TelephoneTYPE(String name) {
        this.fullName = name;
    }

}
