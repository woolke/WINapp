package pl.windykacjasamochodowa.store.model._enums.raport;

import lombok.Getter;

@Getter
public enum RaportTYPE {
    BRAK_KONTAKTU ("Brak kontaktu"),
    KREDYTOBIORCA ("Kredytobiorca"),
    PORECZYCIEL ("Poręczyciel"),
    PELNOMOCNIK ("Pełnomocnik"),
    UZYTKOWNIK ("Użytkownik"),
    WARSZTAT ("Warsztat"),
    RODZINA ("Rodzina"),
    DOMOWNIK ("Domownik"),
    OSOBA_TRZECIA ("Osoba trzecia"),
    SKIP ("SKIP");

    private final String fullName;

    RaportTYPE(String fullName) {
        this.fullName = fullName;
    }
}
