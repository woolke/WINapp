package pl.windykacjasamochodowa.store.model._enums;

import lombok.Getter;

@Getter
public enum WinPARTNER {
    KI ("Kredyt Inkaso") ,
    EGB ("Egb investmens"),
    GETBACK ("GetBack"),
    PRAGROUP ("PraGroup"),;

    private final String fullName;

    WinPARTNER(String fullName) {
        this.fullName = fullName;
    }
}
