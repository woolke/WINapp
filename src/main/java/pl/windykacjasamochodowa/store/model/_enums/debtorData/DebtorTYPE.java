package pl.windykacjasamochodowa.store.model._enums.debtorData;

import lombok.Getter;

@Getter
public enum DebtorTYPE {
    ND ("-"),
    OSOBA ("Osoba prywatna"),
    FIRMA ("Firma");

    private final String fullName;

    DebtorTYPE(String fullName) {
        this.fullName = fullName;
    }

    public static DebtorTYPE mappingAndValueOf (String string) {
        if (string.equals("Osoba fizyczna")) {
            string = "OSOBA";
        } else if (string.equals("Firma")) {
            string ="FIRMA";
        }
        return DebtorTYPE.valueOf(string);
    }
}
