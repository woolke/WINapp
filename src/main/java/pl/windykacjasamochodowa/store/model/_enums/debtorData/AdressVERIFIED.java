package pl.windykacjasamochodowa.store.model._enums.debtorData;

import lombok.Getter;

@Getter
public enum AdressVERIFIED {
    ND ("-", 0),
    ZWERYFIKOWANY_OK ("Aktualny", 3),
    ZWERYFIKOWANY_CZASOWA ("Nieobecność czasowa", 3),
    ZWERYFIKOWANY_DLUGOTRWALA ("Nieobecność długotrwała", 3),
    NIEZWERYFIKOWANY_LOW ("Mało prawdopodobny", 2),
    NIEZWERYFIKOWANY_HIGH ("Prawdopodobny",1),
    NIEZWERYFIKOWANY("Niezweryfikowany",1),
    NIEAKTUALNY ("Nieaktualny", 0 );

    String fullName;
    Integer value;

    AdressVERIFIED(String fullName, Integer value) {
        this.fullName = fullName;
        this.value = value;
    }
}
