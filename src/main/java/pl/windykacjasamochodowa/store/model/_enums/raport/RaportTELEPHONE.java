package pl.windykacjasamochodowa.store.model._enums.raport;

import lombok.Getter;

@Getter
public enum RaportTELEPHONE {
    ND ("-"),
    ODEBRANA ("Rozmowa"),
    NIE_ODBIERA (" Nie odbiera"),
    ZAJETE ("Zajęte"),
    POCZTA ("Poczta"),
    NTN ("Nie ma takiego numeru");


    private final String fullName;



    RaportTELEPHONE(String fullName) {
        this.fullName = fullName;
    }
}



