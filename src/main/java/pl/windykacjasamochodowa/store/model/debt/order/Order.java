package pl.windykacjasamochodowa.store.model.debt.order;

import lombok.*;
import org.hibernate.validator.constraints.Length;
import pl.windykacjasamochodowa.store.model._enums.WinPARTNER;
import pl.windykacjasamochodowa.store.model.debt.debitors.Debtor;
import pl.windykacjasamochodowa.store.model.raportAndComments.OrderComment;
import pl.windykacjasamochodowa.store.services.OrderService;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "orders")
public class Order {
    @Id
    String forum;

    @ManyToMany (cascade = CascadeType.ALL)
    Set<Debtor> debtors;

    @OneToMany (mappedBy = "order", cascade = CascadeType.ALL)
    Set<Payment> payments = new HashSet<Payment>();

    @OneToMany (mappedBy = "order", cascade = CascadeType.ALL)
    Set<Balance> balances = new HashSet<Balance>();

    @OneToMany (mappedBy = "order", cascade = CascadeType.ALL)
    Set<OrderDetails> orderDetails = new HashSet<OrderDetails>();


    @OneToOne (cascade = CascadeType.ALL)
    Car car;

    @OneToOne (cascade = CascadeType.ALL)
    Mortgage mortgage;
    //
    @ManyToOne
    OrderType orderType;

    @Enumerated(EnumType.STRING)
    WinPARTNER winPARTNER = getWinPARTNER();

    String orginalCreditor;
    String orginalNumer;
    String orginalIBAN;
    String actualIBAN;
    String variant;
    String orderManager;

    @Length(max = 5000)
    String orderComments;


    @Temporal(TemporalType.DATE)
    Date signingDate;

    @Temporal(TemporalType.DATE)
    Date terminationDate;

    @Temporal(TemporalType.DATE)
    Date maturityDate2;

    public String printIBAN () {
        return OrderService.printIBAN(actualIBAN);
    }

    public String printOrginalIBAN () {
        return OrderService.printIBAN(orginalIBAN);
    }

    public String getTerminationAndMaturityDate() {
        Date returnString;
        if (terminationDate==null) {
            returnString  = maturityDate2;
        } else {
            returnString = terminationDate;
        }
        if (returnString == null) {
            return "";
        } else {
            return returnString.toString();
        }

    }


}
