package pl.windykacjasamochodowa.store.model.debt.order;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table (name = "balances")
public class Balance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @ManyToOne (cascade = CascadeType.ALL)
    Order order;

    BigDecimal balance;
    BigDecimal capital;
    BigDecimal interest;
    BigDecimal costs;

    @Temporal(TemporalType.DATE)
    Date balanceDate;

    String comment;


}
