package pl.windykacjasamochodowa.store.model.debt.assigment;

import lombok.*;
import pl.windykacjasamochodowa.store.model.Role;

import javax.persistence.*;
import java.util.Set;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "vindicators")
public class Vindicator {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    String username;
    String fullName;
    String email;

    String password;

    @ManyToMany
    private Set<Role> roles;

    public boolean isManager() {
        return roles.stream().anyMatch(roles -> roles.getName().equals("MANAGER"));
    }

}
