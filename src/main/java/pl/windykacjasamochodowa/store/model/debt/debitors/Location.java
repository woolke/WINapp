package pl.windykacjasamochodowa.store.model.debt.debitors;


import lombok.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.data.geo.Point;

import javax.persistence.*;
import java.net.URL;
import java.util.Objects;
import java.util.Scanner;
import java.util.Set;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "locations")
public class Location {
    @Id
    @Column(name = "IdPoint")
    Point id;

    Double lat;
    Double lng;


    @OneToMany(mappedBy = "location")
    Set<Adress> adresses;

    @Override
    public boolean equals(Object o) {
        Point oPoint = (Point)o;
        return (id.getX()==oPoint.getX()) && (id.getY()==oPoint.getY());
    }

}