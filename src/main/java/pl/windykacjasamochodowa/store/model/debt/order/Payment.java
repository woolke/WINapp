package pl.windykacjasamochodowa.store.model.debt.order;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table (name = "payments")
public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    BigDecimal value;


    @ManyToOne
    Order order;

    String payer;
    Boolean toSettlement;

    @Temporal(TemporalType.DATE)
    Date WT_SettlementDate;

    @Temporal(TemporalType.DATE)
    Date WIN_SettlementDate;

}
