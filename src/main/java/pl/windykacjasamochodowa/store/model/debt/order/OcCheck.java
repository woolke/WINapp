package pl.windykacjasamochodowa.store.model.debt.order;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "oc_checks")
public class OcCheck {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn(name = "nrRej")
    Car car;

    String brandAndModel;
    String technicalExam;
    String mileage;
    String ocPolcy;
    String lastCoOwner;
    String status;
    String ufgByVin;
    String ufgByRej;
    String stolen;
    String type;

    @Temporal(TemporalType.DATE)
    Date checkDate;

}
