package pl.windykacjasamochodowa.store.model.debt.order;

import lombok.*;
import org.apache.el.parser.BooleanNode;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "cars")
public class Car {

    @Id
    private String nrRej;

    @OneToOne(mappedBy = "car")
    private Order order;

    @OneToMany (mappedBy = "car")
    private Set<OcCheck> ocChecs;

    @Temporal(TemporalType.DATE)
    private Date firstRejDate;

    private String oldNrRej;
    private String vin;
    private String brand;
    private String model;
    private String productionYear;
    private String type;
    private String additionalComments;

    private String contractOfTransfer;
    private String terminationDoc;
    private String confirmationOfReceipt;

    @Override
    public String toString() {
        String modelPrint="";
        String brandPrint="";
        if (model != null) {
            modelPrint=model;
        }
        if (brand != null) {
            brandPrint=brand;
        }
        return nrRej + " (" + brandPrint + " " + modelPrint +")";
    }
}
