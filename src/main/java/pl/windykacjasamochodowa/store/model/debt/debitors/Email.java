package pl.windykacjasamochodowa.store.model.debt.debitors;

import lombok.Data;
import pl.windykacjasamochodowa.store.model._enums.debtorData.EmailSOURCE;

import javax.persistence.*;

@Data
@Entity
@Table(name = "emails")
public class Email {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String email;
    String comment;
    Boolean verified;

    @ManyToOne
    Subject subject;


    @Enumerated (EnumType.STRING)
    public EmailSOURCE emailSOURCE =getEmailSOURCE();

}
