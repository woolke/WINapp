package pl.windykacjasamochodowa.store.model.debt.debitors;

import lombok.*;
import pl.windykacjasamochodowa.store.model._enums.debtorData.AdressSOURCE;
import pl.windykacjasamochodowa.store.model._enums.debtorData.AdressTYPE;
import pl.windykacjasamochodowa.store.model._enums.debtorData.AdressVERIFIED;
import pl.windykacjasamochodowa.store.model.raportAndComments.AdresComment;

import javax.persistence.*;
import java.util.Set;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "adresses")
public class Adress {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @OneToMany(mappedBy = "adress", cascade = CascadeType.ALL)
    Set<AdresComment> adressComments;

    @ManyToOne (cascade = CascadeType.ALL)
    Location location;

    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn(name = "subject_id")
    Subject subject;


    String adressFull;
    String anotherAdressForGeolocation;
    String adressZipCode;
    String adressCity;
    String adressStreet;
    String adressHouseNumber;
    String adressDoorNumber;
    String comment;
    Boolean toMap;


    @Enumerated(EnumType.STRING)
    public AdressTYPE adressTYPE = getAdressTYPE();

    @Enumerated(EnumType.STRING)
    public AdressSOURCE adressSOURCE = getAdressSOURCE();

    @Enumerated(EnumType.STRING)
    public AdressVERIFIED adressVERIFIED = getAdressVERIFIED();


    public String printFullAdress() {
        String adresToPrint;
        if (adressFull==null)
            adresToPrint = adressZipCode+ " " + adressCity +", "
                    +adressStreet + " " + adressHouseNumber + adressDoorNumber;
        else {
            adresToPrint = adressFull;
        }
        return adresToPrint;

    }
}
