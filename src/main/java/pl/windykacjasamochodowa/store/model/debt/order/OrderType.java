package pl.windykacjasamochodowa.store.model.debt.order;


import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "orderType")
public class OrderType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToMany (mappedBy = "forum")
    Set<Order> orders;

    private String name;
    private Double winAmountSalaryRate;
    private Double wtAmountSalaryRate;
    private Double winRetriveSalary;
    private Double wtRetriveSalary;



}
