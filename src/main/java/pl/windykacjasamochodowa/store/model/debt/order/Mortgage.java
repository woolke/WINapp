package pl.windykacjasamochodowa.store.model.debt.order;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table (name = "mortgages")
public class Mortgage {
    @Id
    String nrKW;
}
