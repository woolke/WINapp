package pl.windykacjasamochodowa.store.model.debt.assigment;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table (name = "campaigns")
public class Campaign {
    @Id
    String name;

    @Column
    @Temporal(TemporalType.DATE)
    Date campaignStart;

    @Column
    @Temporal(TemporalType.DATE)
    Date campaignEnd;
}
