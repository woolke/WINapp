package pl.windykacjasamochodowa.store.model.debt.debitors;

import lombok.*;
import pl.windykacjasamochodowa.store.model._enums.debtorData.DebtorROLE;
import pl.windykacjasamochodowa.store.model._enums.debtorData.DebtorTYPE;
import pl.windykacjasamochodowa.store.model.debt.order.Order;

import javax.persistence.*;
import java.util.Set;


@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "debtors")
public class Debtor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @ManyToOne (cascade = CascadeType.ALL)
    Subject subject;

    @ManyToMany (mappedBy = "debtors", cascade = CascadeType.ALL)
    Set<Order> orders;

    @Enumerated(EnumType.STRING)
    DebtorTYPE debtorTYPE = getDebtorTYPE();

    @Enumerated(EnumType.STRING)
    DebtorROLE debtorROLE = getDebtorROLE();
}
