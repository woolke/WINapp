package pl.windykacjasamochodowa.store.model.debt.debitors;

import lombok.*;
import pl.windykacjasamochodowa.store.model._enums.debtorData.TelephoneSOURCE;
import pl.windykacjasamochodowa.store.model._enums.debtorData.TelephoneTYPE;

import javax.persistence.*;

import static pl.windykacjasamochodowa.store.services.TelephoneService.prettyPrintTelephone;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
public class Telephone {

    @Override
    public String toString() {
        return telephone;
    }

    @Id
    @GeneratedValue (strategy = GenerationType.SEQUENCE)
    Integer id;

    String telephone;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "subject_id")
    Subject subject;

    String comment;
    Boolean verified;

    @Enumerated(EnumType.STRING)
    public TelephoneTYPE telephoneTYPE = getTelephoneTYPE();

    @Enumerated(EnumType.STRING)
    public TelephoneSOURCE telephoneSOURCE = getTelephoneSOURCE();

    public String prettyPrint() {
        return prettyPrintTelephone(this);
    }


}
