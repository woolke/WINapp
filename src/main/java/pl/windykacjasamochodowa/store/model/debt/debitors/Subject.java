package pl.windykacjasamochodowa.store.model.debt.debitors;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;


@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "subjects")
public class Subject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subject subject = (Subject) o;
        return (Objects.equals(pesel, subject.pesel) && !pesel.isEmpty()) ||
                (Objects.equals(nip, subject.nip) && !pesel.isEmpty()) ||
                (Objects.equals(regon, subject.regon) && !regon.isEmpty());
    }


    @OneToMany(mappedBy = "subject", cascade = CascadeType.ALL)
    Set<Telephone> telephones;

    @OneToMany(mappedBy = "subject", cascade = CascadeType.ALL)
    Set<Adress> adresses;

    @OneToMany(mappedBy = "subject", cascade = CascadeType.ALL)
    Set<Email> emails;

    @OneToMany(mappedBy = "subject", cascade = CascadeType.ALL)
    Set<Debtor> debtors;

    String pesel;
    String nip;
    String regon;
    String name;
    String firstName;
    String lastName;

    public String printPeselNipRegon() {
        String printString = "";
        if (pesel != null) {
            if (pesel.contains("brak")) {
                printString = "brak";
            } else {
                printString = printString.concat("PESEL:").concat(pesel).concat("; ");
            }
        }
        if (nip != null) {
            printString = printString.concat("NIP:").concat(nip);
        }
        if (regon != null) {
            printString = printString.concat("REGON:").concat(regon);
        }
        return printString;
    }

    public String printName() {
        String printString = "";
        if (firstName != null) {
            printString = printString.concat(firstName).concat(" ");
        }
        if (lastName != null) {
            printString = printString.concat(lastName).concat(" ");
        }
        if (name != null) {
            printString = printString.concat("|").concat(name).concat(" ").concat("|");
        }
        return printString;
    }
}
