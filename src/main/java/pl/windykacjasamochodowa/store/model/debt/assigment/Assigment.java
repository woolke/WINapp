package pl.windykacjasamochodowa.store.model.debt.assigment;

import lombok.*;
import pl.windykacjasamochodowa.store.dtos.RaportDto;
import pl.windykacjasamochodowa.store.model.debt.debitors.Debtor;
import pl.windykacjasamochodowa.store.model.debt.order.Order;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "assigments")
public class Assigment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @ManyToOne (cascade = CascadeType.ALL)
    Vindicator vindicator;

    @ManyToOne (cascade = CascadeType.ALL)
    Order order;

    @ManyToOne (cascade = CascadeType.ALL)
    Campaign campaign;

    @Temporal(TemporalType.DATE)
    Date orderStart;

    @Temporal(TemporalType.DATE)
    Date orderEnd;

    @Temporal(TemporalType.DATE)
    Date declarationRetrieveDate;

    @Temporal(TemporalType.DATE)
    Date declarationPaymantDate;

    @Temporal(TemporalType.DATE)
    Date scheduledTask;

    BigDecimal declarationPaymantValue;

    public ArrayList<RaportDto> getDtosToForumRaport () {
        ArrayList<RaportDto> raportDtos = new ArrayList<>();
        for (Debtor debtor : this.getOrder().getDebtors()) {
            RaportDto buildDto = RaportDto.builder()
                    .order(this.getOrder())
                    .forum(this.getOrder().getForum())
                    .subject(debtor.getSubject())
                    .debtorRole(debtor.getDebtorROLE().getFullName())
                    .car(this.getOrder().getCar().getNrRej()).build();
            raportDtos.add(buildDto);
        }

        return raportDtos;
    }

}
